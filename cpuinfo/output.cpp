// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "output.hpp"

#include <algorithm>
#include <map>

class Output::Implementation {
public:
    Implementation(
            Output& output,
            Output::Format format,
            Output::Color color) noexcept :
        m_output { output },
        m_format { format },
        m_color { color },
        m_indent_string { "    " } {
    }

    Implementation(const Implementation&) noexcept = delete;

    Implementation(Implementation&&) noexcept = delete;

    Implementation& operator=(const Implementation&) noexcept = delete;

    Implementation& operator=(Implementation&&) noexcept = delete;

    virtual ~Implementation() = default;

    [[nodiscard]] const Output::Format& format() const noexcept {
        return m_format;
    }

    [[nodiscard]] const Output::Color& color() const noexcept {
        return m_color;
    }

    [[nodiscard]] const std::string& indent_string() const noexcept {
        return m_indent_string;
    }

    [[nodiscard]] std::string& indent_string() noexcept {
        return m_indent_string;
    }

    [[nodiscard]] int& indent_level() noexcept {
        return m_indent_level;
    }

    [[nodiscard]] Output& output() noexcept {
        return m_output;
    }

    void color_key() const noexcept {
        if (m_color == Output::colored) {
            m_output << "\033[0;94m";
        }
    }

    void color_value() const noexcept {
        if (m_color == Output::colored) {
            m_output << "\033[0;97m";
        }
    }

    void color_decorum() const noexcept {
        if (m_color == Output::colored) {
            m_output << "\033[0;90m";
        }
    }

    void color_reset() const noexcept {
        if (m_color == Output::colored) {
            m_output << "\033[0m";
        }
    }

    void print_blank(size_t width) const noexcept {
        print_decorum(" ", width);
    }

    void print_newline() const noexcept {
        m_output << '\n';
        for (int i = 0; i < m_indent_level; ++i) {
            m_output << m_indent_string;
        }
    }

    void print_decorum(
            const std::string& string,
            size_t count = 1) const noexcept {
        color_decorum();
        for (size_t i = 0; i < count; ++i) {
            m_output << string;
        }
        color_reset();
    }

    void print_decorum(const std::string& string, bool count) const noexcept {
        if (count) {
            print_decorum(string);
        }
    }

    virtual void print_key(const std::string& value) = 0;

    virtual void print_value(const std::string& value, bool literal) = 0;

    virtual void print_list(
            const std::vector<List::element_type>& elements) = 0;

    virtual void print_map(
            const std::vector<Map::element_type>& elements) = 0;

    virtual void print_table(
            const std::vector<Table::element_type>& elements,
            bool show_index) = 0;

private:
    Output& m_output;
    Output::Format m_format;
    Output::Color m_color;
    std::string m_indent_string;
    int m_indent_level {};
};

class FormatterText : public Output::Implementation {
public:
    using Output::Implementation::Implementation;

    FormatterText(const FormatterText&) noexcept = delete;

    FormatterText(FormatterText&&) noexcept = delete;

    FormatterText& operator=(const FormatterText&) noexcept = delete;

    FormatterText& operator=(FormatterText&&) noexcept = delete;

    ~FormatterText() override = default;

    void print_key(const std::string& value) override {
        color_key();
        output() << value;
        color_reset();
    }

    void print_value(const std::string& value, bool /* literal */) override {
        color_value();
        output() << value;
        color_reset();
    }

    void print_list(const std::vector<List::element_type>& elements) override {
        if (elements.empty()) {
            return;
        }

        for (size_t i = 0; i < elements.size(); ++i) {
            if (i != 0) {
                print_newline();
            }

            elements[i]->print(output());
        }
    }

    void print_map(const std::vector<Map::element_type>& elements) override {
        if (elements.empty()) {
            return;
        }

        auto width = elements.front().first.length();
        for (const auto& element : elements) {
            width = std::max(width, element.first.size());
        }

        for (size_t i = 0; i < elements.size(); ++i) {
            if (i != 0) {
                print_newline();
            }

            const auto& [key, value] = elements[i];

            print_key(key);
            print_decorum(": ");

            print_blank(width - key.length());

            indent_level() += 1;
            if (dynamic_cast<const Value*>(value.get()) == nullptr) {
                print_newline();
            }
            value->print(output());
            indent_level() -= 1;
        }
    }

    void print_table(
            const std::vector<Table::element_type>& elements,
            bool show_index) override {
        if (elements.empty()) {
            return;
        }

        auto col_width = std::map<std::string, size_t> {};
        for (const auto& row : elements) {
            for (const auto& [key, val] : row) {
                col_width[key] = std::max(col_width[key], key.length());
                col_width[key] = std::max(col_width[key], val->data().length());
            }
        }

        auto index_width = std::to_string(elements.size()).length();

        if (show_index) {
            print_blank(index_width);
            print_decorum(" | ");
        }

        for (auto it = col_width.begin(); it != col_width.end(); ++it) {
            print_decorum(" | ", it != col_width.begin());
            print_key(it->first);
            print_blank(it->second - it->first.length());
        }

        print_newline();

        if (show_index) {
            print_decorum("-", index_width);
            print_decorum("-+-");
        }

        for (auto it = col_width.begin(); it != col_width.end(); ++it) {
            print_decorum("-+-", it != col_width.begin());
            print_decorum("-", it->second);
        }

        for (size_t c = 0; c < elements.size(); ++c) {
            print_newline();

            if (show_index) {
                auto index = std::to_string(c);
                print_key(index);
                print_blank(index_width - index.length());
                print_decorum(" | ");
            }

            auto entries = std::map<std::string, Value*> {};
            for (const auto& [key, value] : elements[c]) {
                entries[key] = value.get();
            };

            for (auto row = col_width.begin(); row != col_width.end(); ++row) {
                print_decorum(" | ", row != col_width.begin());

                const auto& key = row->first;
                const auto& width = row->second;

                if (auto it = entries.find(key); it != entries.end()) {
                    auto& value = it->second;
                    print_value(value->data(), value->literal());
                    print_blank(width - value->data().length());
                } else {
                    print_blank(width);
                }
            }
        }
    }
};

class FormatterJson : public Output::Implementation {
public:
    using Output::Implementation::Implementation;

    FormatterJson(const FormatterJson&) noexcept = delete;

    FormatterJson(FormatterJson&&) noexcept = delete;

    FormatterJson& operator=(const FormatterJson&) noexcept = delete;

    FormatterJson& operator=(FormatterJson&&) noexcept = delete;

    ~FormatterJson() override = default;

    void print_key(const std::string& value) override {
        color_key();
        // encode "\"" -> "\\\"", and "\\" -> "\\\\"
        output() << '"';
        for (const auto& c : value) {
            if (c == '"' || c == '\\') {
                output() << '\\';
            }
            output() << c;
        }
        output() << '"';
        color_reset();
    }

    void print_value(const std::string& value, bool literal) override {
        color_value();
        // encode "\"" -> "\\\"", and "\\" -> "\\\\"
        if (literal) {
            output() << value;
        } else {
            output() << '"';
            for (const auto& c : value) {
                if (c == '"' || c == '\\') {
                    output() << '\\';
                }
                output() << c;
            }
            output() << '"';
        }
        color_reset();
    }

    void print_list(const std::vector<List::element_type>& elements) override {
        if (elements.empty()) {
            print_decorum("[]");
            return;
        }

        indent_level() += 1;
        print_decorum("[");
        print_newline();

        for (size_t i = 0; i < elements.size(); ++i) {
            if (i != 0) {
                print_decorum(",");
                print_newline();
            }

            elements[i]->print(output());
        }

        indent_level() -= 1;
        print_newline();
        print_decorum("]");
    }

    void print_map(const std::vector<Map::element_type>& elements) override {
        if (elements.empty()) {
            print_decorum("{}");
            return;
        }

        print_decorum("{");
        indent_level() += 1;
        print_newline();

        for (size_t i = 0; i < elements.size(); ++i) {
            if (i != 0) {
                print_decorum(",");
                print_newline();
            }

            const auto& [key, value] = elements[i];
            print_key(key);
            print_decorum(": ");
            value->print(output());
        }

        indent_level() -= 1;
        print_newline();
        print_decorum("}");
    }

    void print_table(
            const std::vector<Table::element_type>& elements,
            bool /* show_index */) override {
        if (elements.empty()) {
            print_decorum("[]");
            return;
        }

        print_decorum("[");
        indent_level() += 1;
        print_newline();

        for (size_t r = 0; r < elements.size(); ++r) {
            if (r != 0) {
                print_decorum(",");
                print_newline();
            }

            const auto& column = elements[r];
            if (column.empty()) {
                print_decorum("{}");
                continue;
            }

            print_decorum("{");
            indent_level() += 1;
            print_newline();

            for (size_t c = 0; c < column.size(); ++c) {
                if (c != 0) {
                    print_decorum(",");
                    print_newline();
                }

                const auto& [key, value] = column[c];

                print_key(key);
                print_decorum(": ");
                print_value(value->data(), value->literal());
            }

            indent_level() -= 1;
            print_newline();
            print_decorum("}");
        }

        indent_level() -= 1;
        print_newline();
        print_decorum("]");
    }
};

class FormatterXml : public Output::Implementation {
public:
    using Output::Implementation::Implementation;

    FormatterXml(const FormatterXml&) noexcept = delete;

    FormatterXml(FormatterXml&&) noexcept = delete;

    FormatterXml& operator=(const FormatterXml&) noexcept = delete;

    FormatterXml& operator=(FormatterXml&&) noexcept = delete;

    ~FormatterXml() override = default;

    void print_key(const std::string& value) override {
        color_key();
        // no perfect encoding, replace non-alpha-numerical characters with "_"

        for (size_t i = 0; i < value.size(); ++i) {
            auto c = value[i];

            if ((i == 0) && (c >= '0' && c <= '9')) {
                output() << ':' << c;
                continue;
            }

            auto is_safe =
                    (c >= 'a' && c <= 'z') || //
                    (c >= 'A' && c <= 'Z') || //
                    (c == '_' || c == ':') || //
                    ((i != 0) && (c >= '0' && c <= '9')) || //
                    ((i != 0) && (c == '-' || c == '.'));

            output() << (is_safe ? c : '_');
        }

        color_reset();
    }

    void print_value(const std::string& value, bool /* literal */) override {
        color_value();
        // encode "<" -> "&lt;", ">" -> "&gt;", and "&" -> "&amp;"
        for (const auto& c : value) {
            if (c == '<') {
                output() << "&lt;";
            } else if (c == '>') {
                output() << "&gt;";
            } else if (c == '&') {
                output() << "&amp;";
            } else {
                output() << c;
            }
        }
        color_reset();
    }

    void print_list(const std::vector<List::element_type>& elements) override {
        if (elements.empty()) {
            print_decorum("<list/>");
            return;
        }

        indent_level() += 1;
        print_decorum("<list>");

        for (const auto& element : elements) {
            print_newline();
            print_decorum("<element>");
            element->print(output());
            print_decorum("</element>");
        }

        indent_level() -= 1;
        print_newline();
        print_decorum("</list>");
    }

    void print_map(const std::vector<Map::element_type>& elements) override {
        if (elements.empty()) {
            print_decorum("<map/>");
            return;
        }

        print_decorum("<map>");
        indent_level() += 1;

        for (const auto& element : elements) {
            print_newline();

            print_decorum("<");
            print_key(element.first);
            print_decorum(">");

            element.second->print(output());

            print_decorum("</");
            print_key(element.first);
            print_decorum(">");
        }

        indent_level() -= 1;
        print_newline();
        print_decorum("</map>");
    }

    void print_table(
            const std::vector<Table::element_type>& elements,
            bool show_index) override {
        if (elements.empty()) {
            print_decorum("<table/>");
            return;
        }

        print_decorum("<table>");
        indent_level() += 1;

        for (size_t rowid = 0; rowid < elements.size(); ++rowid) {
            print_newline();
            auto rowstr = show_index ? std::to_string(rowid) : "row";

            print_decorum("<");
            print_key(rowstr);
            print_decorum(">");

            indent_level() += 1;

            for (const auto& [key, value] : elements[rowid]) {
                print_newline();
                print_decorum("<");
                print_key(key);
                print_decorum(">");

                print_value(value->data(), value->literal());

                print_decorum("</");
                print_key(key);
                print_decorum(">");
            }

            indent_level() -= 1;
            print_newline();
            print_decorum("</");
            print_key(rowstr);
            print_decorum(">");
        }

        indent_level() -= 1;
        print_newline();
        print_decorum("</table>");
    }
};

Output::Output(std::ostream& stream, Format format, Color color) :
    m_stream { stream } {
    switch (format) {
    case text:
        m_impl = std::make_unique<FormatterText>(*this, format, color);
        break;

    case json:
        m_impl = std::make_unique<FormatterJson>(*this, format, color);
        break;

    case xml:
        m_impl = std::make_unique<FormatterXml>(*this, format, color);
        break;

    default:
        break;
    }
}

Output::~Output() = default;

const Output::Format& Output::format() const noexcept {
    return m_impl->format();
}

const Output::Color& Output::color() const noexcept {
    return m_impl->color();
}

const std::string& Output::indent_string() const noexcept {
    return m_impl->indent_string();
}

std::string& Output::indent_string() noexcept {
    return m_impl->indent_string();
}

Output& Output::operator<<(const Value& rhs) noexcept {
    m_impl->print_value(rhs.data(), rhs.literal());
    return *this;
}

Output& Output::operator<<(const List& rhs) noexcept {
    m_impl->print_list(rhs.elements());
    return *this;
}

Output& Output::operator<<(const Map& rhs) noexcept {
    m_impl->print_map(rhs.elements());
    return *this;
}

Output& Output::operator<<(const Table& rhs) noexcept {
    m_impl->print_table(rhs.elements(), rhs.show_row_index());

    return *this;
}

void List::add(Printable* printable) noexcept {
    if (m_sort_key.empty() || printable->sort_key() < m_sort_key) {
        m_sort_key = printable->sort_key();
    }

    m_elements.emplace_back(printable);
    std::sort(m_elements.begin(), m_elements.end(), [](auto&& lhs, auto&& rhs) {
        return lhs->sort_key() < rhs->sort_key();
    });
}

Map* List::add_map(const bool& sort) {
    auto element = new Map { sort };
    add(element);
    return element;
}

void Map::add(const std::string& key, Printable* printable) noexcept {
    if (m_sort_key.empty() || key < m_sort_key) {
        m_sort_key = key;
    }

    for (auto& element : m_elements) {
        if (element.first == key) {
            element.second.reset(printable);
            return;
        }
    }

    m_elements.emplace_back(key, std::unique_ptr<Printable> { printable });
    if (m_sort) {
        auto comparison = [](auto&& lhs, auto&& rhs) {
            return lhs.first < rhs.first;
        };
        std::sort(m_elements.begin(), m_elements.end(), comparison);
    }
}

List* Map::add_list(const std::string& key, const bool& sort) {
    auto element = new List { sort };
    add(key, element);
    return element;
}

Map* Map::add_map(const std::string& key, const bool& sort) {
    auto element = new Map { sort };
    add(key, element);
    return element;
}

void Table::add(size_t row, const std::string& col, Value* printable) noexcept {
    if (m_sort_key.empty() || col < m_sort_key) {
        m_sort_key = col;
    }

    if (row >= m_elements.size()) {
        m_elements.resize(row + 1);
    }

    for (auto& element : m_elements[row]) {
        if (element.first == col) {
            element.second.reset(printable);
            return;
        }
    }

    m_elements[row].emplace_back(std::make_pair(col, printable));
}
