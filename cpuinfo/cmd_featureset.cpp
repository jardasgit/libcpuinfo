// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"
#include <getopt.h>
#include <iostream>
#include <memory>

static constexpr auto usage_string = std::string_view {
    "Usage: cpuinfo featureset [OPTIONS] COMMAND\n"
    "Manipulate featuresets.\n"
    "\n"
    "Options:\n"
    "  -h, --help\n"
    "        Display this help and exit.\n"
    "\n"
    "Available commands:\n"
    "  add [FILE|FEATURE]...\n"
    "        Print the set-union of the specified featuresets and features.\n"
    "  all [FAMILY]\n"
    "        Create a featureset for architecture family FAMILY that contains\n"
    "        all features of this architecture family. If FAMILY is omitted,\n"
    "        the host architecture family is assumed.\n"
    "  arch FILE\n"
    "        Print the architecture of the specified featureset.\n"
    "  baseline FILE...\n"
    "        Reads featuresets from FILES and print a featureset that is the\n"
    "        set-intersection of those sets.\n"
    "  contains FILE [FILE|FEATURE]...\n"
    "        Reads a featureset from FILE and checks whether all FEATURES\n"
    "        are present in the first featureset and all featuresets are a\n"
    "        subset of the first featureset. Returns a status code of zero\n"
    "        if all FEATURES are present, otherwise exits with a non-zero\n"
    "        status code.\n"
    "  empty FILE\n"
    "        Reads a featureset from FILE and checks whether the featureset\n"
    "        is empty. Returns a status code of zero if the featureset is\n"
    "        empty, otherwise exits with a non-zero status code and print\n"
    "        the features in the featureset to stdout. Use this command to\n"
    "        enumerate the features in a featureset.\n"
    "  equal FILE FILE...\n"
    "        Reads two or more featuresets and checks for equality. Returns\n"
    "        a status code of zero if the featuresets are equal, otherwise\n"
    "        exits with a non-zero status code.\n"
    "  host\n"
    "        Create a featureset with the features supported by this machine.\n"
    "  none [FAMILY]\n"
    "        Create a featureset for architecture family FAMILY that contains\n"
    "        no features. If FAMILY is omitted, the host architecture family\n"
    "        is assumed.\n"
    "  remove FILE [FILE|FEATURE]...\n"
    "        Print the first featureset with all following features and\n"
    "        featuresets removed.\n"
};

static int cmd_featureset_help(Output& output) {
    output << usage_string;
    return 0;
}

static int cmd_featureset_add(Output& output, char* argv[]) {
    if (argv[0] == nullptr) {
        cmd_featureset_help(output);
        return 1;
    }

    auto lhs = std::unique_ptr<cpuinfo_featureset_t, Deleter> {};

    for (size_t i = 0; argv[i] != nullptr; ++i) {
        auto rhs = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
            cpuinfo_featureset_xml_set(argv[i])
        };
        if (!rhs) {
            auto feature = std::unique_ptr<cpuinfo_feature_t, Deleter> {
                cpuinfo_feature_by_name(nullptr, argv[i])
            };
            if (!feature) {
                std::cerr << "Error: " << cpuinfo_error_get() << '\n';
                return 1;
            }

            auto family = std::unique_ptr<cpuinfo_family_t, Deleter> {
                cpuinfo_feature_family_get(feature.get())
            };

            rhs.reset(cpuinfo_featureset_new(family.get()));
            cpuinfo_featureset_feature_add(rhs.get(), feature.get());
        }

        if (!lhs) {
            lhs = std::move(rhs);
            continue;
        }

        auto ret = cpuinfo_featureset_featureset_union(lhs.get(), rhs.get());
        if (ret == nullptr) {
            std::cerr << "Error: " << cpuinfo_error_get() << '\n';
            return 1;
        }

        lhs.reset(ret);
    }

    output << cpuinfo_featureset_xml_get(lhs.get()) << '\n';
    return 0;
}

static int cmd_featureset_all(Output& output, char* argv[]) {
    auto family = std::unique_ptr<cpuinfo_family_t, Deleter> {};

    if (argv[0] == nullptr) {
        family.reset(cpuinfo_family_by_name("host", ""));
        if (!family) {
            std::cerr << "Error: " << cpuinfo_error_get() << '\n';
            return 1;
        }
    } else {
        if (argv[1] != nullptr) {
            cmd_featureset_help(output);
            return 1;
        }

        family.reset(cpuinfo_family_by_name(nullptr, argv[0]));
        if (!family) {
            auto arch = std::unique_ptr<cpuinfo_architecture_t, Deleter> {
                cpuinfo_architecture_by_name(nullptr, argv[0])
            };
            if (!arch) {
                std::cerr << "Error: " << cpuinfo_error_get() << '\n';
                return 1;
            }

            family.reset(cpuinfo_architecture_family_get(arch.get()));
        }
    }

    auto obj = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_all(family.get())
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    output << cpuinfo_featureset_xml_get(obj.get()) << '\n';
    return 0;
}

static int cmd_featureset_arch(Output& output, char* argv[]) {
    if (argv[0] == nullptr || argv[1] != nullptr) {
        cmd_featureset_help(output);
        return 1;
    }

    auto obj = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_xml_set(argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto family = std::unique_ptr<cpuinfo_family_t, Deleter> {
        cpuinfo_featureset_family_get(obj.get())
    };

    auto value = Value { cpuinfo_family_name_get(family.get(), nullptr) };

    output << value << '\n';
    return 0;
}

static int cmd_featureset_baseline(Output& output, char* argv[]) {
    if (argv[0] == nullptr) {
        cmd_featureset_help(output);
        return 1;
    }

    auto lhs = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_xml_set(argv[0])
    };
    if (!lhs) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    for (size_t i = 1; argv[i] != nullptr; ++i) {
        auto rhs = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
            cpuinfo_featureset_xml_set(argv[i])
        };
        if (!rhs) {
            std::cerr << "Error: " << cpuinfo_error_get() << '\n';
            return 1;
        }

        auto ret = cpuinfo_featureset_featureset_intersect(
                lhs.get(),
                rhs.get());
        if (ret == nullptr) {
            std::cerr << "Error: " << cpuinfo_error_get() << '\n';
            return 1;
        }

        lhs.reset(ret);
    }

    output << cpuinfo_featureset_xml_get(lhs.get()) << '\n';
    return 0;
}

static int cmd_featureset_contains(Output& output, char* argv[]) {
    if (argv[0] == nullptr || argv[1] == nullptr) {
        cmd_featureset_help(output);
        return 1;
    }

    auto obj = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_xml_set(argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    for (size_t i = 1; argv[i] != nullptr; ++i) {
        auto feature = std::unique_ptr<cpuinfo_feature_t, Deleter> {
            cpuinfo_feature_by_name(nullptr, argv[i])
        };
        if (feature) {
            auto ret = cpuinfo_featureset_feature_contains(
                    obj.get(),
                    feature.get());
            if (ret < 0) {
                std::cerr << "Error: " << cpuinfo_error_get() << '\n';
                return 1;
            }

            if (ret == CPUINFO_FALSE) {
                output << Value { false } << '\n';
                return 1;
            }

            continue;
        }

        auto featureset = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
            cpuinfo_featureset_xml_set(argv[i])
        };
        if (!featureset) {
            std::cerr << "Error: " << cpuinfo_error_get() << '\n';
            return 1;
        }

        auto result = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
            cpuinfo_featureset_featureset_intersect(obj.get(), featureset.get())
        };
        if (!result) {
            output << Value { false } << '\n';
            return 1;
        }

        auto expected = cpuinfo_featureset_feature_count(featureset.get());
        auto actual = cpuinfo_featureset_feature_count(result.get());
        if (actual < expected) {
            output << Value { false } << '\n';
            return 1;
        }
    }

    output << Value { true } << '\n';
    return 0;
}

static int cmd_featureset_empty(Output& output, char* argv[]) {
    if (argv[0] == nullptr || argv[1] != nullptr) {
        cmd_featureset_help(output);
        return 1;
    }

    auto obj = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_xml_set(argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << "\n";
        return 1;
    }

    auto count = cpuinfo_featureset_feature_count(obj.get());
    if (count != 0) {
        for (size_t i = 0; i < count; ++i) {
            auto feature = std::unique_ptr<cpuinfo_feature_t, Deleter> {
                cpuinfo_featureset_feature_get(obj.get(), i)
            };
            output << cpuinfo_feature_name_get(feature.get(), nullptr) << '\n';
        }
        return 1;
    }

    return 0;
}

static int cmd_featureset_equal(Output& output, char* argv[]) {
    if (argv[0] == nullptr || argv[1] == nullptr) {
        cmd_featureset_help(output);
        return 1;
    }

    auto lhs = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_xml_set(argv[0])
    };
    if (!lhs) {
        std::cerr << "Error: " << cpuinfo_error_get() << "\n";
        return 1;
    }

    for (size_t i = 0; argv[i] != nullptr; ++i) {
        auto rhs = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
            cpuinfo_featureset_xml_set(argv[0])
        };
        if (!rhs) {
            std::cerr << "Error: " << cpuinfo_error_get() << "\n";
            return 1;
        }

        auto ret = cpuinfo_featureset_equals(lhs.get(), rhs.get());
        if (ret < 0) {
            std::cerr << "Error: " << cpuinfo_error_get() << "\n";
            return 1;
        }

        if (ret == CPUINFO_FALSE) {
            return 1;
        }
    }

    return 0;
}

int cmd_featureset_host(Output& output, char* argv[]) {
    if (argv[0] != nullptr) {
        cmd_featureset_help(output);
        return 1;
    }

    auto obj = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_host()
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    output << cpuinfo_featureset_xml_get(obj.get()) << '\n';
    return 0;
}

int cmd_featureset_none(Output& output, char* argv[]) {
    auto family = std::unique_ptr<cpuinfo_family_t, Deleter> {};

    if (argv[0] == nullptr) {
        family.reset(cpuinfo_family_by_name("host", ""));
        if (!family) {
            std::cerr << "Error: " << cpuinfo_error_get() << '\n';
            return 1;
        }
    } else {
        if (argv[1] != nullptr) {
            cmd_featureset_help(output);
            return 1;
        }

        family.reset(cpuinfo_family_by_name(nullptr, argv[0]));
        if (!family) {
            auto arch = std::unique_ptr<cpuinfo_architecture_t, Deleter> {
                cpuinfo_architecture_by_name(nullptr, argv[0])
            };
            if (!arch) {
                std::cerr << "Error: " << cpuinfo_error_get() << '\n';
                return 1;
            }

            family.reset(cpuinfo_architecture_family_get(arch.get()));
        }
    }

    auto obj = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_new(family.get())
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    std::cout << cpuinfo_featureset_xml_get(obj.get()) << '\n';
    return 0;
}

static int cmd_featureset_remove(Output& output, char* argv[]) {
    if (argv[0] == nullptr || argv[1] == nullptr) {
        cmd_featureset_help(output);
        return 1;
    }

    auto lhs = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
        cpuinfo_featureset_xml_set(argv[0])
    };
    if (!lhs) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    for (size_t i = 1; argv[i] != nullptr; ++i) {
        auto feature = std::unique_ptr<cpuinfo_feature_t, Deleter> {
            cpuinfo_feature_by_name(nullptr, argv[i])
        };
        if (feature) {
            auto ret = cpuinfo_featureset_feature_remove(
                    lhs.get(),
                    feature.get());
            if (ret < 0) {
                std::cerr << "Error: " << cpuinfo_error_get() << '\n';
                return 1;
            }

            continue;
        }

        auto featureset = std::unique_ptr<cpuinfo_featureset_t, Deleter> {
            cpuinfo_featureset_xml_set(argv[i])
        };
        if (!featureset) {
            std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        }

        auto result = cpuinfo_featureset_featureset_subtract(
                lhs.get(),
                featureset.get());
        if (result == nullptr) {
            std::cerr << "Error: " << cpuinfo_error_get() << '\n';
            return 1;
        }

        lhs.reset(result);
    }

    std::cout << cpuinfo_featureset_xml_get(lhs.get()) << '\n';
    return 0;
}

int cmd_featureset(Output& output, int argc, char* argv[]) {
    auto opt_short = "+:h";
    option opt_long[] = {
        { "help", no_argument, nullptr, 'h' },
        { nullptr, 0, nullptr, 0 }
    };

    auto arg = -1;
    auto idx = -1;

    // NOLINTNEXTLINE: getopt_long is not thread safe
    while ((arg = getopt_long(argc, argv, opt_short, opt_long, &idx)) != -1) {
        switch (arg) {
        case 'h':
            return cmd_featureset_help(output);

        case ':':
            std::cerr << "Error: Missing argument for option\n";
            return 1;

        default:
            std::cerr << "Error: Invalid option\n";
            return 1;
        }
    }

    if (optind == argc) {
        cmd_featureset_help(output);
        return 1;
    }

    auto cmd = std::string { argv[optind] };
    optind += 1;

    if (cmd == "add") {
        return cmd_featureset_add(output, argv + optind);
    }

    if (cmd == "all") {
        return cmd_featureset_all(output, argv + optind);
    }

    if (cmd == "arch") {
        return cmd_featureset_arch(output, argv + optind);
    }

    if (cmd == "baseline") {
        return cmd_featureset_baseline(output, argv + optind);
    }

    if (cmd == "contains") {
        return cmd_featureset_contains(output, argv + optind);
    }

    if (cmd == "empty") {
        return cmd_featureset_empty(output, argv + optind);
    }

    if (cmd == "equal") {
        return cmd_featureset_equal(output, argv + optind);
    }

    if (cmd == "host") {
        return cmd_featureset_host(output, argv + optind);
    }

    if (cmd == "none") {
        return cmd_featureset_none(output, argv + optind);
    }

    if (cmd == "remove") {
        return cmd_featureset_remove(output, argv + optind);
    }

    cmd_featureset_help(output);
    return 1;
}
