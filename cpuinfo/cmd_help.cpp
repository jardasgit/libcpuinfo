// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"

static constexpr auto usage_string = std::string_view {
    "Usage: cpuinfo [OPTIONS]... COMMAND\n"
    "Show and manipulate CPU information.\n"
    "\n"
    "Options:\n"
    "  -h, --help\n"
    "        Alias for command 'help'.\n"
    "  -v, --version\n"
    "        Alias for command 'version'.\n"
    "  --color on|off|auto\n"
    "        Enable color output. Defaults to 'auto'.\n"
    "  --format text|json|xml\n"
    "       Select output format. Defaults to 'text'.\n"
    "\n"
    "Available commands:\n"
    "  help\n"
    "        Display this help and exit. Use 'cpuinfo COMMAND --help' to get\n"
    "        help for a specific command.\n"
    "  version\n"
    "        Display version information and exit.\n"
    "  endianness\n"
    "        Query known endiannesses.\n"
    "  family\n"
    "        Query known architecture families.\n"
    "  architecture\n"
    "        Query known architectures.\n"
    "  feature\n"
    "        Query known features.\n"
    "  featureset\n"
    "        Manipulate feature sets.\n"
    "  cpuid\n"
    "        [x86] Query cpuid information.\n"
    "  msr\n"
    "        [x86] Read and write machine specific registers.\n"
};

int cmd_help(Output& output, int /* argc */, char* /* argv */[]) {
    output << usage_string;
    return 0;
}
