// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"

#include <getopt.h>
#include <iostream>
#include <libcpuinfo/arch/x86.h>

static constexpr auto usage_string = std::string_view {
    "Usage: cpuinfo feature [OPTIONS] [FEATURE [KEY]]\n"
    "Query known features.\n"
    "\n"
    "Options:\n"
    "  -h, --help\n"
    "        Display this help and exit.\n"
    "\n"
    "Available commands:\n"
    "  feature\n"
    "        Show all known features.\n"
    "  feature FEATURE\n"
    "        Show all known information for feature FEATURE.\n"
    "  feature FEATURE (name|description|family|features|hostsupport)\n"
    "        Show information for feature.\n"
};

static Value hex(Output& output, size_t value, int width = 8) {
    if (output.format() == Output::text) {
        return Value { to_hex(value, width), true };
    }

    return Value { value };
}

static int cmd_feature_help(Output& output) {
    output << usage_string;
    return 0;
}

static int cmd_feature_entry(Output& output, char* argv[]) {
    auto obj = std::unique_ptr<cpuinfo_feature_t, Deleter> {
        cpuinfo_feature_by_name(nullptr, argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto element = std::string { argv[1] };
    if (element == "name") {
        output << Value { cpuinfo_feature_name_get(obj.get(), nullptr) };
    } else if (element == "description") {
        output << Value { cpuinfo_feature_description_get(obj.get()) };
    } else if (element == "family") {
        auto family = std::unique_ptr<cpuinfo_family_t, Deleter> {
            cpuinfo_feature_family_get(obj.get())
        };
        output << Value { cpuinfo_family_name_get(family.get(), nullptr) };
    } else if (element == "features") {
        auto list = List { true };
        auto count_features = cpuinfo_feature_features_count(obj.get());
        for (size_t i = 0; i < count_features; ++i) {
            auto feature = std::unique_ptr<cpuinfo_feature_t, Deleter> {
                cpuinfo_feature_features_get(obj.get(), i)
            };
            list.add_value(cpuinfo_feature_name_get(feature.get(), nullptr));
        }
        output << list;
    } else if (element == "hostsupport") {
        output << Value { cpuinfo_feature_hostsupport_get(obj.get()) == 1 };
    } else {
        std::cerr << "Error: Unknown element '" << element << "'\n";
        return 1;
    }

    output << '\n';
    return 0;
}

static int cmd_feature_show(Output& output, char* argv[]) {
    auto obj = std::unique_ptr<cpuinfo_feature_t, Deleter> {
        cpuinfo_feature_by_name(nullptr, argv[0])
    };
    if (!obj) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto map = Map { false };
    map.add_value("name", cpuinfo_feature_name_get(obj.get(), nullptr));

    auto count_aliases = cpuinfo_feature_alias_count(obj.get());
    if (count_aliases != 0) {
        auto list = map.add_list("aliases");
        for (size_t i = 0; i < count_aliases; ++i) {
            auto alias = std::unique_ptr<cpuinfo_alias_t, Deleter> {
                cpuinfo_feature_alias_get(obj.get(), i)
            };

            auto string = std::string {};
            string += cpuinfo_alias_domain_get(alias.get());
            string += ':';
            string += cpuinfo_alias_name_get(alias.get());
            list->add_value(string);
        }
    }

    map.add_value("description", cpuinfo_feature_description_get(obj.get()));

    auto family = std::unique_ptr<cpuinfo_family_t, Deleter> {
        cpuinfo_feature_family_get(obj.get())
    };
    if (family) {
        map.add_value("family", cpuinfo_family_name_get(family.get(), nullptr));
    }

    auto count_features = cpuinfo_feature_features_count(obj.get());
    if (count_features > 0) {
        auto list = map.add_list("features", true);
        for (size_t i = 0; i < count_features; ++i) {
            auto feature = std::unique_ptr<cpuinfo_feature_t, Deleter> {
                cpuinfo_feature_features_get(obj.get(), i)
            };
            list->add_value(cpuinfo_feature_name_get(feature.get(), nullptr));
        }
    }

    auto eax_in = uint32_t {};
    auto ecx_in = uint32_t {};
    auto index = uint32_t {};
    auto eax = uint32_t {};
    auto ebx = uint32_t {};
    auto ecx = uint32_t {};
    auto edx = uint32_t {};

    auto has_cpuid = cpuinfo_feature_x86_extra_cpuid_get(
            obj.get(),
            &eax_in,
            &ecx_in,
            &eax,
            &ebx,
            &ecx,
            &edx);
    if (has_cpuid == 0) {
        auto cpuid_map = map.add_map("x86 cpuid", false);
        cpuid_map->add_value("eax_in", hex(output, eax_in));
        cpuid_map->add_value("ecx_in", hex(output, ecx_in, 2));
        cpuid_map->add_value("eax", hex(output, eax));
        cpuid_map->add_value("ebx", hex(output, ebx));
        cpuid_map->add_value("ecx", hex(output, ecx));
        cpuid_map->add_value("edx", hex(output, edx));
    }

    auto has_msr = cpuinfo_feature_x86_extra_msr_get(
            obj.get(),
            &index,
            &eax,
            &edx);
    if (has_msr == 0) {
        auto msr_map = map.add_map("x86 msr", false);
        msr_map->add_value("index", hex(output, index));
        msr_map->add_value("eax", hex(output, eax));
        msr_map->add_value("edx", hex(output, edx));
    }

    map.add_value(
            "hostsupport",
            cpuinfo_feature_hostsupport_get(obj.get()) == 1);

    output << map << '\n';
    return 0;
}

static int cmd_feature_list(Output& output) {
    auto list = List { true };

    auto size = cpuinfo_feature_count();
    for (size_t i = 0; i < size; ++i) {
        auto obj = std::unique_ptr<cpuinfo_feature_t, Deleter> {
            cpuinfo_feature_by_index(i)
        };
        list.add_value(cpuinfo_feature_name_get(obj.get(), nullptr));
    }

    output << list << '\n';
    return 0;
}

int cmd_feature(Output& output, int argc, char* argv[]) {
    auto opt_short = "+:h";
    option opt_long[] = {
        { "help", no_argument, nullptr, 'h' },
        { nullptr, 0, nullptr, 0 }
    };

    auto arg = -1;
    auto idx = -1;

    // NOLINTNEXTLINE: getopt_long is not thread safe
    while ((arg = getopt_long(argc, argv, opt_short, opt_long, &idx)) != -1) {
        switch (arg) {
        case 'h':
            return cmd_feature_help(output);

        case ':':
            std::cerr << "Error: Missing argument for option\n";
            return 1;

        default:
            std::cerr << "Error: Invalid option\n";
            return 1;
        }
    }

    switch (argc - optind) {
    case 0:
        return cmd_feature_list(output);

    case 1:
        return cmd_feature_show(output, argv + optind);

    case 2:
        return cmd_feature_entry(output, argv + optind);

    default:
        break;
    }

    return cmd_feature_help(output);
}
