// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"

#include <getopt.h>
#include <iostream>
#include <libcpuinfo/arch/x86.h>
#include <map>

static constexpr auto usage_string = std::string_view {
    "Usage: cpuinfo cpuid [OPTIONS] [FAMILY [KEY]]\n"
    "Query x86 cpuinfo information.\n"
    "\n"
    "Options:\n"
    "  -h, --help\n"
    "        Display this help and exit.\n"
    "\n"
    "Available commands:\n"
    "  level\n"
    "        Print the highest id of any basic leaf.\n"
    "  xlevel\n"
    "        Print the highest id of any extended leaf.\n"
    "  [LEAF [SUBLEAF [(eax|ebx|ecx|edx] ] ]\n"
    "        Print information about the specified cpuid leaf.\n"
    "\n"
    "If no command is specified, outputs a table of all cpuid leaves.\n"
};

static Value hex(Output& output, size_t value, int width = 8) {
    if (output.format() == Output::text) {
        return Value { to_hex(value, width), true };
    }

    return Value { value };
}

static int cmd_cpuid_help(Output& output) {
    output << usage_string;
    return 0;
}

struct cmd_cpuid_list_callback_data {
    Output& output;
    Table table {};
    size_t index {};
};

static int cmd_cpuid_list_callback(
        uint32_t in_eax,
        uint32_t in_ecx,
        uint32_t out_eax,
        uint32_t out_ebx,
        uint32_t out_ecx,
        uint32_t out_edx,
        void* ptr) {
    auto data = static_cast<cmd_cpuid_list_callback_data*>(ptr);
    data->index += 1;

    data->table.add_value(data->index, "in_eax", hex(data->output, in_eax));
    data->table.add_value(data->index, "in_ecx", hex(data->output, in_ecx, 2));
    data->table.add_value(data->index, "out_eax", hex(data->output, out_eax));
    data->table.add_value(data->index, "out_ebx", hex(data->output, out_ebx));
    data->table.add_value(data->index, "out_ecx", hex(data->output, out_ecx));
    data->table.add_value(data->index, "out_edx", hex(data->output, out_edx));
    return 0;
}

static int cmd_cpuid_table(Output& output) {
    auto data = cmd_cpuid_list_callback_data { output };

    if (cpuinfo_x86_cpuid_foreach(cmd_cpuid_list_callback, &data) != 0) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
    }

    output << data.table << '\n';
    return 0;
}

static int cmd_cpuid_leaf(Output& output, uint32_t in_eax, uint32_t in_ecx) {
    uint32_t out_eax {};
    uint32_t out_ebx {};
    uint32_t out_ecx {};
    uint32_t out_edx {};

    auto ret = cpuinfo_x86_cpuid(
            in_eax,
            in_ecx,
            &out_eax,
            &out_ebx,
            &out_ecx,
            &out_edx);

    if (ret != 0) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto map = Map {};
    map.add_value("eax", hex(output, out_eax));
    map.add_value("ebx", hex(output, out_ebx));
    map.add_value("ecx", hex(output, out_ecx));
    map.add_value("edx", hex(output, out_edx));

    output << map << '\n';
    return 0;
}

static int cmd_cpuid_leaf(
        Output& output,
        uint32_t in_eax,
        uint32_t in_ecx,
        int reg) {
    uint32_t out_reg[4] {};

    auto ret = cpuinfo_x86_cpuid(
            in_eax,
            in_ecx,
            &out_reg[0],
            &out_reg[1],
            &out_reg[2],
            &out_reg[3]);

    if (ret != 0) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    output << hex(output, out_reg[reg]) << '\n';
    return 0;
}

int cmd_cpuid(Output& output, int argc, char* argv[]) {
    using namespace std::literals;

    auto opt_short = "+:h";
    option opt_long[] = {
        { "help", no_argument, nullptr, 'h' },
        { nullptr, 0, nullptr, 0 }
    };

    auto arg = -1;
    auto idx = -1;

    // NOLINTNEXTLINE: getopt_long is not thread safe
    while ((arg = getopt_long(argc, argv, opt_short, opt_long, &idx)) != -1) {
        switch (arg) {
        case 'h':
            return cmd_cpuid_help(output);

        case ':':
            std::cerr << "Error: Missing argument for option\n";
            return 1;

        default:
            std::cerr << "Error: Invalid option\n";
            return 1;
        }
    }

    auto num_arguments = argc - optind;

    if (num_arguments == 0) {
        return cmd_cpuid_table(output);
    }

    if (num_arguments == 1 && "level"sv == argv[optind]) {
        return cmd_cpuid_leaf(output, CPUINFO_X86_CPUID_LEAF_BASIC, 0, 0);
    }

    if (num_arguments == 1 && "xlevel"sv == argv[optind]) {
        return cmd_cpuid_leaf(output, CPUINFO_X86_CPUID_LEAF_EXTENDED, 0, 0);
    }

    auto in_eax = uint32_t {};
    try {
        in_eax = std::stoul(argv[optind], nullptr, 0);
    } catch (const std::exception&) {
        cmd_cpuid_help(output);
        return 1;
    }

    if (num_arguments == 1) {
        return cmd_cpuid_leaf(output, in_eax, 0);
    }

    auto in_ecx = uint32_t {};
    try {
        in_ecx = std::stoul(argv[optind + 1], nullptr, 0);
    } catch (const std::exception&) {
        cmd_cpuid_help(output);
        return 1;
    }

    if (num_arguments == 2) {
        return cmd_cpuid_leaf(output, in_eax, in_ecx);
    }

    if (num_arguments == 3) {
        auto regs = std::map<std::string, int> {
            { "eax", 0 },
            { "ebx", 1 },
            { "ecx", 2 },
            { "edx", 3 },
        };

        auto it = regs.find(argv[optind + 2]);
        if (it == regs.end()) {
            cmd_cpuid_help(output);
            return 1;
        }

        return cmd_cpuid_leaf(output, in_eax, in_ecx, it->second);
    }

    cmd_cpuid_help(output);
    return 1;
}
