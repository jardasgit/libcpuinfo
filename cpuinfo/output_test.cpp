// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "output.hpp"

#include <gtest/gtest.h>
#include <map>
#include <sstream>
#include <string>
#include <vector>

class TestData {
public:
    TestData(
            const char* name,
            std::shared_ptr<Printable> value,
            const char* text,
            const char* json,
            const char* xml) noexcept :
        m_name { name },
        m_value { std::move(value) },
        m_text { text },
        m_json { json },
        m_xml { xml } {
    }

    [[nodiscard]] const std::string& name() const {
        return m_name;
    }

    [[nodiscard]] std::string suit() const {
        if (dynamic_cast<const Value*>(m_value.get()) != nullptr) {
            return "Value";
        }

        if (dynamic_cast<const List*>(m_value.get()) != nullptr) {
            return "List";
        }

        if (dynamic_cast<const Map*>(m_value.get()) != nullptr) {
            return "Map";
        }

        if (dynamic_cast<const Table*>(m_value.get()) != nullptr) {
            return "Table";
        }

        throw std::runtime_error { "unknown printable type" };
    }

    [[nodiscard]] std::string actual(Output::Format format) const {
        auto stream = std::stringstream {};
        auto output = Output { stream, format, Output::monochrome };

        m_value->print(output);

        return stream.str();
    }

    [[nodiscard]] const std::string& expected(Output::Format format) const {
        switch (format) {
        case Output::text:
            return m_text;

        case Output::json:
            return m_json;

        case Output::xml:
            return m_xml;
        }

        throw std::runtime_error { "unknown output format" };
    }

private:
    std::string m_name;
    std::shared_ptr<Printable> m_value;
    std::string m_text;
    std::string m_json;
    std::string m_xml;
};

class MyTest : public testing::Test {
public:
    explicit MyTest(const TestData& testData, Output::Format format) :
        m_testData { testData },
        m_format { format } {
    }

    void TestBody() override {
        auto expected = m_testData.expected(m_format);
        auto actual = m_testData.actual(m_format);

        EXPECT_EQ(expected, actual);
    }

private:
    const TestData& m_testData;
    Output::Format m_format;
};

static std::vector<TestData> generate_tests() {
    auto value_false = std::make_shared<Value>(false);

    auto value_true = std::make_shared<Value>(true);

    auto value_pos = std::make_shared<Value>(7);

    auto value_neg = std::make_shared<Value>(-42);

    auto value_float = std::make_shared<Value>(123.45);

    auto value_string = std::make_shared<Value>("foo<>&\"\\");

    auto list_0 = std::make_shared<List>();

    auto list_1 = std::make_shared<List>();
    list_1->add_value(0);

    auto list_2 = std::make_shared<List>();
    list_2->add_value(0);
    list_2->add_value(1);

    auto list_nested = std::make_shared<List>();
    list_nested->add_value(0);
    auto list_nested_map = list_nested->add_map();
    list_nested_map->add_value("1", "a");
    list_nested_map->add_value("2", "b");
    list_nested->add_value(3);

    auto map_0 = std::make_shared<Map>();

    auto map_1 = std::make_shared<Map>();
    map_1->add_value("a", 0);

    auto map_2 = std::make_shared<Map>();
    map_2->add_value("a", 0);
    map_2->add_value("foo", 1);

    auto map_nested = std::make_shared<Map>();
    map_nested->add_value("a", 0);
    auto map_nested_list = map_nested->add_list("b");
    map_nested_list->add_value(1);
    map_nested_list->add_value(2);
    auto nested_map = map_nested->add_map("c");
    nested_map->add_value("d", 42);

    auto table_0 = std::make_shared<Table>();

    auto table_2 = std::make_shared<Table>();
    table_2->add_value(1, "a", "x");
    table_2->add_value(1, "b", "y");
    table_2->add_value(2, "b", "foo");

    // clang-format off
    return {
        {
            "BoolFalse",
            value_false,
            "false",
            "false",
            "false"
        }, {
            "BoolTrue",
            value_true,
            "true",
            "true",
            "true",
        }, {
            "PosInt",
            value_pos,
            "7",
            "7",
            "7"
        }, {
            "NegInt",
            value_neg,
            "-42",
            "-42",
            "-42"
        }, {
            "Float",
            value_float,
            "123.450000",
            "123.450000",
            "123.450000"
        }, {
            "String",
            value_string,
            "foo<>&\"\\",
            "\"foo<>&\\\"\\\\\"",
            "foo&lt;&gt;&amp;\"\\"
        }, {
            "Empty",
            list_0,
            "",
            "[]",
            "<list/>"
        }, {
            "One",
            list_1,
            "0",
            "[\n    0\n]",
            "<list>\n    <element>0</element>\n</list>"
        }, {
            "Two",
            list_2,
            "0\n1",
            "[\n    0,\n    1\n]",
            "<list>\n    <element>0</element>\n    <element>1</element>\n"
                "</list>"
        }, {
            "Nested",
            list_nested,
            "0\n1: a\n2: b\n3",
            "[\n    0,\n    {\n        \"1\": \"a\",\n        \"2\": \"b\"\n"
                "    },\n    3\n]",
            "<list>\n    <element>0</element>\n    <element><map>\n"
                "        <:1>a</:1>\n        <:2>b</:2>\n    </map></element>\n"
                "    <element>3</element>\n</list>"
        }, {
            "Empty",
            map_0,
            "",
            "{}",
            "<map/>"
        }, {
            "One",
            map_1,
            "a: 0",
            "{\n    \"a\": 0\n}",
            "<map>\n    <a>0</a>\n</map>"
        }, {
            "Two",
            map_2,
            "a:   0\nfoo: 1",
            "{\n    \"a\": 0,\n    \"foo\": 1\n}",
            "<map>\n    <a>0</a>\n    <foo>1</foo>\n</map>"
        }, {
            "Nested",
            map_nested,
            "a: 0\nb: \n    1\n    2\nc: \n    d: 42",
            "{\n    \"a\": 0,\n    \"b\": [\n        1,\n        2\n    ],\n"
                "    \"c\": {\n        \"d\": 42\n    }\n}",
            "<map>\n    <a>0</a>\n    <b><list>\n        <element>1</element>\n"
                "        <element>2</element>\n    </list></b>\n    <c><map>\n"
                "        <d>42</d>\n    </map></c>\n</map>"
        }, {
            "Empty",
            table_0,
            "",
            "[]",
            "<table/>"
        }, {
            "TwoByTwo",
            table_2,
            "a | b  \n--+----\nx | y  \n  | foo",
            "[\n    {\n        \"a\": \"x\",\n        \"b\": \"y\"\n    },\n"
                "    {\n        \"b\": \"foo\"\n    }\n]",
            "<table>\n    <row>\n        <a>x</a>\n        <b>y</b>\n"
                "    </row>\n    <row>\n        <b>foo</b>\n    </row>\n"
                "</table>"
        },
    };
    // clang-format on
}

static int main_wrapped(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);

    auto formats = std::map<Output::Format, std::string> {
        { Output::text, "Text" },
        { Output::json, "Json" },
        { Output::xml, "Xml" },
    };

    auto testset = generate_tests();

    for (const auto& data : testset) {
        for (const auto& [format, format_name] : formats) {
            auto test = new MyTest { data, format };
            testing::RegisterTest(
                    data.suit().c_str(),
                    (data.name() + '/' + format_name).c_str(),
                    nullptr,
                    nullptr,
                    __FILE__,
                    __LINE__,
                    [=]() { return test; });
        }
    }

    return RUN_ALL_TESTS();
}

int main(int argc, char* argv[]) {
    try {
        return main_wrapped(argc, argv);
    } catch (const std::exception& e) {
        std::cerr << e.what();
    }

    return 1;
}
