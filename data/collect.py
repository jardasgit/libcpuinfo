#!/usr/bin/env python3

import argparse
import lxml.etree
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", type=os.path.realpath)
    parser.add_argument("file", type=os.path.realpath, nargs="+")
    args = parser.parse_args()

    rootname = {
        "architecture.xml": "architectures",
        "endianness.xml": "endiannesses",
        "family.xml": "families",
        "feature.xml": "features",
    }.get(os.path.basename(args.output))

    parser = lxml.etree.XMLParser(remove_comments=True, remove_blank_text=True)
    root = lxml.etree.Element(rootname)

    for filename in args.file:
        with open(filename, "tr") as f:
            doc = lxml.etree.parse(f, parser=parser)
        for node in doc.getroot():
            root.append(node)

    text = lxml.etree.tostring(
        root,
        encoding="UTF-8",
        xml_declaration=True,
        pretty_print=True)

    with open(args.output, "tw") as f:
        f.write(text.decode())


if __name__ == "__main__":
    main()
