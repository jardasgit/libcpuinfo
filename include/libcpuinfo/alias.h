// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

/*
 * The alias system is best explained by an example:
 *
 * Assume two entities with canonical names "foo" and "bar" that are also often
 * called "foo" and "baz". Some third party program "Program" calls the
 * entities "bar" and "baz" though. This would be modeled in XML like so:
 *
 * <entity>
 *   <name>foo</name>
 *   <aliases>
 *     <alias><name>bar</name><domain>program</domain></alias>
 *   </aliases>
 * </entity>
 * <entity>
 *   <name>bar</name>
 *   <aliases>
 *     <alias><name>baz</name><domain/></alias>
 *   </aliases>
 * </entity>
 *
 * Finding the entity with cpuinfo_entity_by_name(name, domain)
 * ============================================================
 *
 * Setting domain to NULL will only match the canonical name:
 * cpuinfo_entity_by_name("foo", NULL)                  // returns foo
 * cpuinfo_entity_by_name("bar", NULL)                  // returns bar
 * cpuinfo_entity_by_name("baz", NULL)                  // returns NULL
 *
 * Setting domain to an (possibly empty) string allows aliases to match:
 * cpuinfo_entity_by_name("foo", "")                    // returns foo
 * cpuinfo_entity_by_name("bar", "")                    // returns bar
 * cpuinfo_entity_by_name("baz", "")                    // returns bar
 *
 * Setting the domain will prefer perfect alias matches over names:
 * cpuinfo_entity_by_name("foo", "program");            // returns foo
 * cpuinfo_entity_by_name("bar", "program")             // returns foo
 * cpuinfo_entity_by_name("baz", "program");            // returns bar
 *
 * An unknown domain is identical to setting it to an empty string.
 *
 * Setting the name to NULL will interpret domain as "domain:name":
 * "bar"        -> same as cpuinfo_entity_by_name("bar", "");
 * ":bar"       -> same as cpuinfo_entity_by_name("bar", "");
 * "foo:bar"    -> same as cpuinfo_entity_by_name("bar", "foo");
 * "foo:"       -> same as cpuinfo_entity_by_name("", "foo");
 *
 *
 * Retrieving the entity's name with cpuinfo_entity_name_get(obj, domain)
 * ======================================================================
 *
 * Retrieving the entity's canonical name:
 * cpuinfo_entity_name_get(foo, NULL);                  // returns "foo"
 *
 * The entity's name in unknown / empty domains would be its canonical name:
 * cpuinfo_entity_name_get(foo, "foobar");              // returns "foo"

 * The entity's name in the "program" domain would be "bar":
 * cpuinfo_entity_name_get(foo, "program");             // returns "bar"
 *
 *
 *
 * Querying alias information of an entity
 * =======================================
 *
 * Retrieving the number of aliases of an entity:
 * cpuinfo_entity_alias_count(foo);                     // returns 1
 *
 * Retrieving alias information:
 * alias = cpuinfo_entity_alias_get(foo, 0);
 * cpuinfo_alias_domain_get(alias);                     // returns "program"
 * cpuinfo_alias_name_get(alias);                       // returns "bar"
 * cpuinfo_alias_free(alias);
 *
 * cpuinfo_entity_alias_get(e, 1);                      // returns NULL
 */

#pragma once

#include <libcpuinfo/common.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * Release a cpuinfo_alias_t object.
 *
 * No-op if `obj` is NULL.
 *
 * @param obj object to release.
 */
extern void cpuinfo_alias_free(cpuinfo_alias_t* obj);

/**
 * Returns the alias' domain.
 *
 * The returned string is internally allocated and must not be freed.
 *
 * @param obj alias.
 * @return domain string or NULL on error.
 */
extern const char* cpuinfo_alias_domain_get(cpuinfo_alias_t* obj);

/**
 * Returns the alias' name.
 *
 * The returned string is internally allocated and must not be freed.
 *
 * @param obj alias.
 * @return name string or NULL on error.
 */
extern const char* cpuinfo_alias_name_get(cpuinfo_alias_t* obj);

/**
 * Return an object's xml representation.
 *
 * The returned string must be released by the application
 * using `cpuinfo_xml_free()`.
 *
 * @param obj object.
 * @return object's xml representation or `NULL` on error.
 */
extern char* cpuinfo_alias_xml_get(cpuinfo_alias_t* obj);

/**
 * Checks two alias objects for equality.
 *
 * @param lhs first object.
 * @param rhs second object.
 * @return `CPUINFO_TRUE` if both objects are equal, `CPUINFO_FALSE` otherwise,
 *     and a negative value on error.
 */
extern int cpuinfo_alias_equals(cpuinfo_alias_t* lhs, cpuinfo_alias_t* rhs);

#ifdef __cplusplus
}
#endif /* __cplusplus */
