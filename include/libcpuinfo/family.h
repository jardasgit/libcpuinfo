// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <libcpuinfo/common.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * Count number of known architecture families.
 *
 * @return number of known architecture families.
 */
extern size_t cpuinfo_family_count(void);

/**
 * Retrieve an architecture family by its index in the pool.
 *
 * @param index index into the pool.
 * @return the `index`th entry in the pool or `NULL` on error.
 */
extern cpuinfo_family_t* cpuinfo_family_by_index(size_t index);

/**
 * Retrieve an architecture family by its name.
 *
 * @param name name or alias of the object.
 * @param domain domain in which this name is used.
 * @return the requested object or `NULL` on error.
 * @see alias.h
 */
extern cpuinfo_family_t* cpuinfo_family_by_name(
        const char* name,
        const char* domain);

/**
 * Release a cpuinfo_family_t object.
 *
 * No-op if `obj` is NULL.
 *
 * @param obj object to release.
 */
extern void cpuinfo_family_free(cpuinfo_family_t* obj);

/**
 * Retrieve object's name.
 *
 * The returned string is internally allocated and must not be freed.
 *
 * @param obj object
 * @param domain alias domain or `NULL` for the canonical name.
 * @return the object's name or `NULL` on error.
 */
extern const char* cpuinfo_family_name_get(
        cpuinfo_family_t* obj,
        const char* domain);

/**
 * Count the number of alias names.
 *
 * @param obj object
 * @return the number of alias names.
 */
extern size_t cpuinfo_family_alias_count(cpuinfo_family_t* obj);

/**
 * Return alias information for an object.
 *
 * @param obj object
 * @param index index into the object's list of alias names.
 * @returns object's `index`th alias name or `NULL` on error.
 */
extern cpuinfo_alias_t* cpuinfo_family_alias_get(
        cpuinfo_family_t* obj,
        size_t index);

/**
 * Return object's description.
 *
 * The returned string is internally allocated and must not be freed.
 *
 * @param obj object
 * @return object's description or `NULL` on error.
 */
extern const char* cpuinfo_family_description_get(cpuinfo_family_t* obj);

/**
 * Return an object's xml representation.
 *
 * The returned string must be released by the application
 * using `cpuinfo_xml_free()`.
 *
 * @param obj object.
 * @return object's xml representation or `NULL` on error.
 */
extern char* cpuinfo_family_xml_get(cpuinfo_family_t* obj);

/**
 * Checks two architecture family objects for equality.
 *
 * @param lhs first object.
 * @param rhs second object.
 * @return `CPUINFO_TRUE` if both objects are equal, `CPUINFO_FALSE` otherwise,
 *     and a negative value on error.
 */
extern int cpuinfo_family_equals(cpuinfo_family_t* lhs, cpuinfo_family_t* rhs);

#ifdef __cplusplus
}
#endif /* __cplusplus */
