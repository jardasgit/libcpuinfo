// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <libcpuinfo/common.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * Create a new cpu feature set of the given architecture family that contains
 * no cpu features.
 *
 * @param family architecture family.
 * @return new cpu feature set or `NULL` on error.
 */
extern cpuinfo_featureset_t* cpuinfo_featureset_new(cpuinfo_family_t* family);

/**
 * Create a new cpu feature set of the given architecture family that contains
 * all available cpu features for that architecture family.
 *
 * @param family architecture family.
 * @return new cpu feature set or `NULL` on error.
 */
extern cpuinfo_featureset_t* cpuinfo_featureset_all(cpuinfo_family_t* family);

/**
 * Create a new cpu feature set representing the host machines capabilities.
 * @return new cpu feature set or `NULL` on error.
 */
extern cpuinfo_featureset_t* cpuinfo_featureset_host(void);

/**
 * Copies a cpu feature set.
 *
 * @param obj object.
 * @return new copy of the cpu feature set or `NULL` on error.
 */
extern cpuinfo_featureset_t* cpuinfo_featureset_clone(
        cpuinfo_featureset_t* obj);

/**
 * Release a cpuinfo_featureset_t object.
 *
 * No-op if `obj` is NULL.
 *
 * @param obj object to release.
 */
extern void cpuinfo_featureset_free(cpuinfo_featureset_t* obj);

/**
 * Checks two cpu feature sets for equality.
 *
 * @param lhs first object.
 * @param rhs second object.
 * @return `CPUINFO_TRUE` if both objects are equal, `CPUINFO_FALSE` otherwise,
 *     and a negative value on error.
 */
extern int cpuinfo_featureset_equals(
        cpuinfo_featureset_t* lhs,
        cpuinfo_featureset_t* rhs);

/**
 * Return object's architecture family.
 *
 * @param obj object.
 * @return object's architecture family or `NULL` on error.
 */
extern cpuinfo_family_t* cpuinfo_featureset_family_get(
        cpuinfo_featureset_t* obj);

/**
 * Count number of other cpu features in this cpu feature set.
 *
 * @param obj object.
 * @return number of cpu features or `0` on error.
 */
extern size_t cpuinfo_featureset_feature_count(cpuinfo_featureset_t* obj);

/**
 * Retrieve a cpu feature in this cpu feature set.
 *
 * @param obj object.
 * @param index index into the the object's list of cpu features.
 * @return the `index`th entry in the list or `NULL` on error.
 */
extern cpuinfo_feature_t* cpuinfo_featureset_feature_get(
        cpuinfo_featureset_t* obj,
        size_t index);

/**
 * Add a cpu feature to this cpu feature set.
 *
 * @param obj object.
 * @param feature cpu feature to add.
 * @return `CPUINFO_FALSE` if the cpu feature was already included in the cpu
 *     feature set, `CPUINFO_TRUE` otherwise, and a negative value on error.
 */
extern int cpuinfo_featureset_feature_add(
        cpuinfo_featureset_t* obj,
        cpuinfo_feature_t* feature);

/**
 * Remove a cpu feature from this cpu feature set.
 *
 * @param obj object.
 * @param feature cpu feature to remove.
 * @return `CPUINFO_FALSE` if the cpu feature was not included in the cpu
 *     feature set, `CPUINFO_TRUE` otherwise, and a negative value on error.
 */
extern int cpuinfo_featureset_feature_remove(
        cpuinfo_featureset_t* obj,
        cpuinfo_feature_t* feature);

/**
 * Removes all cpu features from a cpu feature set.
 *
 * @param obj object.
 */
extern void cpuinfo_featureset_feature_clear(cpuinfo_featureset_t* obj);

/**
 * Check whether a cpu feature set contains a given cpu feature.
 *
 * @param obj object.
 * @param feature cpu feature to check.
 * @return `CPUINFO_TRUE` if the cpu feature set contains the cpu feature,
 *     `CPUINFO_FALSE` otherwise, and a negative value on error.
 */
extern int cpuinfo_featureset_feature_contains(
        cpuinfo_featureset_t* obj,
        cpuinfo_feature_t* feature);

/**
 * Create a cpu feature set including all cpu features present in either or
 * both of two cpu feature sets.
 *
 * @param lhs first object.
 * @param rhs second object.
 * @return new cpu feature set that is the set-union of `lhs` and `rhs` or
 *     `NULL` on error.
 */
cpuinfo_featureset_t* cpuinfo_featureset_featureset_union(
        cpuinfo_featureset_t* lhs,
        cpuinfo_featureset_t* rhs);

/**
 * Create a cpu feature set including all cpu features present in both
 * of two cpu feature sets.
 *
 * @param lhs first object.
 * @param rhs second object.
 * @return new cpu feature set that is the set-intersection of `lhs` and `rhs`
 *     or `NULL` on error.
 */
cpuinfo_featureset_t* cpuinfo_featureset_featureset_intersect(
        cpuinfo_featureset_t* lhs,
        cpuinfo_featureset_t* rhs);

/**
 * Create a cpu feature set including all cpu features present in the first
 * cpu feature set but not present in the second cpu feature set.
 *
 * @param lhs first object.
 * @param rhs second object.
 * @return new cpu feature set that is the set-subtraction of `lhs` and `rhs`
 *     or `NULL` on error.
 */
cpuinfo_featureset_t* cpuinfo_featureset_featureset_subtract(
        cpuinfo_featureset_t* lhs,
        cpuinfo_featureset_t* rhs);

/**
 * Create a cpu feature set from an xml representation.
 *
 * @param filename full path to the xml file.
 * @return new cpu feature set or `NULL` on error.
 */
extern cpuinfo_featureset_t* cpuinfo_featureset_xml_set(const char* filename);

/**
 * Return an object's xml representation.
 *
 * The returned string must be released by the application
 * using `cpuinfo_xml_free()`.
 *
 * @param obj object.
 * @return object's xml representation or `NULL` on error.
 */
extern char* cpuinfo_featureset_xml_get(cpuinfo_featureset_t* obj);

#ifdef __cplusplus
}
#endif /* __cplusplus */
