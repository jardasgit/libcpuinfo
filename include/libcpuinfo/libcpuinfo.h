// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <libcpuinfo/common.h>

#include <libcpuinfo/alias.h>
#include <libcpuinfo/architecture.h>
#include <libcpuinfo/endianness.h>
#include <libcpuinfo/family.h>
#include <libcpuinfo/feature.h>
#include <libcpuinfo/featureset.h>
