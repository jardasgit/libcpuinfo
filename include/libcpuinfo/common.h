// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define CPUINFO_FALSE 0
#define CPUINFO_TRUE 1

typedef struct cpuinfo_alias_t cpuinfo_alias_t;
typedef struct cpuinfo_architecture_t cpuinfo_architecture_t;
typedef struct cpuinfo_endianness_t cpuinfo_endianness_t;
typedef struct cpuinfo_family_t cpuinfo_family_t;
typedef struct cpuinfo_feature_t cpuinfo_feature_t;
typedef struct cpuinfo_featureset_t cpuinfo_featureset_t;

#define CPUINFO_VERSION_MAJOR 0
#define CPUINFO_VERSION_MINOR 0
#define CPUINFO_VERSION_PATCH 1

/**
 * Get the version of cpuinfo that is linked with the application.
 *
 * This function can be called safely at any time, even before calling
 * cpuinfo_init().
 *
 * @param major pointer to storage for the version's major component.
 * @param minor pointer to storage for the version's minor component.
 * @param patch pointer to storage for the version's patch component.
 */
extern void cpuinfo_version(int* major, int* minor, int* patch);

/**
 * Initialize the cpuinfo library.
 *
 * @return 0 on success or a negative value on failure.
 */
extern int cpuinfo_init(void);

/**
 * Deinitialize the cpuinfo library.
 *
 * It is safe to call this function even if cpuinfo_init() was not called
 * beforehand or cpuinfo_init() did not return successfully.
 */
extern void cpuinfo_quit(void);

/**
 * Reset per-thread error message.
 */
extern void cpuinfo_error_clear(void);

/**
 * Return the most recent per-thread error message.
 *
 * The returned string is internally allocated and must not be freed.
 *
 * @return the most recen per-thread error message.
 */
extern const char* cpuinfo_error_get(void);

/**
 * Release an object's xml representation.
 *
 * No-op if `obj` is NULL.
 *
 * @param obj object to release.
 */
extern void cpuinfo_xml_free(char* obj);

#if defined(__riscv) \
        || defined(__riscv__)
#define CPUINFO_FAMILY_IS_RISCV 1
#endif

#if defined(__x86_64) \
        || defined(__x86_64__) \
        || defined(__amd64) \
        || defined(__amd64__) \
        || defined(_M_X64) \
        || defined(i386) \
        || defined(__i386) \
        || defined(__i386__) \
        || defined(__i486) \
        || defined(__i486__) \
        || defined(__i586) \
        || defined(__i586__) \
        || defined(__i686) \
        || defined(__i686__) \
        || defined(_M_IX86)
#define CPUINFO_FAMILY_IS_X86 1
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */
