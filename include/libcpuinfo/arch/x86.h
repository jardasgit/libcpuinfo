// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <libcpuinfo/common.h>

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** Start of basic x86 cpuid leaves. */
#define CPUINFO_X86_CPUID_LEAF_BASIC 0x0UL

/** Start of extended x86 cpuid leaves. */
#define CPUINFO_X86_CPUID_LEAF_EXTENDED 0x80000000UL

/**
 * Retrieve x86 cpuid information.
 *
 * @param in_eax cpuid leaf.
 * @param in_ecx cpuid sub leaf.
 * @param out_eax pointer to storage for eax result.
 * @param out_ebx pointer to storage for ebx result.
 * @param out_ecx pointer to storage for ecx result.
 * @param out_edx pointer to storage for edx result.
 * @return 0 on success or a negative value on failure.
 */
int cpuinfo_x86_cpuid(
        uint32_t in_eax,
        uint32_t in_ecx,
        uint32_t* out_eax,
        uint32_t* out_ebx,
        uint32_t* out_ecx,
        uint32_t* out_edx);

/**
 * Callback function for cpuinfo_x86_cpuid_foreach.
 *
 * @param in_eax cpuid leaf
 * @param in_ecx cpuid sub leaf
 * @param out_eax eax result
 * @param out_ebx ebx result
 * @param out_ecx ecx result
 * @param out_edx edx result
 * @param userdata pointer provided to cpuinfo_x86_cpuid_foreach.
 * @return 0 on success or any other value to abort iteration.
 */
typedef int(cpuinfo_x86_cpuid_callback)(
        uint32_t in_eax,
        uint32_t in_ecx,
        uint32_t out_eax,
        uint32_t out_ebx,
        uint32_t out_ecx,
        uint32_t out_edx,
        void* userdata);

/**
 * Iterate over all x86 cpuid leaves.
 *
 * @param cb callback function to call for each x86 cpuid leaf.
 * @param userdata application provided data to pass to callback function.
 * @return 0 on success or last non-zero value returned by cb.
 */
int cpuinfo_x86_cpuid_foreach(cpuinfo_x86_cpuid_callback* cb, void* userdata);

/**
 * Retrieve x86 msr information.
 *
 * @param in_ecx msr index
 * @param out_eax pointer to storage for eax result.
 * @param out_ecx pointer to storage for ecx result.
 * @return 0 on success or a negative value on failure.
 */
int cpuinfo_x86_msr_read(
        uint32_t in_ecx,
        uint32_t* out_eax,
        uint32_t* out_edx);

/**
 * Write x86 msr information.
 *
 * @param in_ecx msr index
 * @param in_eax lower 32 bits of new value.
 * @param in_ecx upper 32 bits of new value.
 * @return 0 on success or a negative value on failure.
 */
int cpuinfo_x86_msr_write(
        uint32_t in_ecx,
        uint32_t in_eax,
        uint32_t in_edx);

/**
 * Retrieve extra x86 information from cpuinfo_feature_t.
 *
 * @param obj object
 * @param eax_in storage for eax_in or NULL.
 * @param ecx_in storage for ecx_in or NULL.
 * @param eax storage for eax or NULL.
 * @param ebx storage for ebx or NULL.
 * @param ecx storage for ecx or NULL.
 * @param edx storage for edx or NULL.
 * @return 0 on success, 1 if no x86-cpuid information is available,
 *     or a negative value on error.
 */
int cpuinfo_feature_x86_extra_cpuid_get(
        cpuinfo_feature_t* obj,
        uint32_t* eax_in,
        uint32_t* ecx_in,
        uint32_t* eax,
        uint32_t* ebx,
        uint32_t* ecx,
        uint32_t* edx);

/**
 * Retrieve extra x86 information from cpuinfo_feature_t.
 *
 * @param obj object
 * @param index storage for eax_in or NULL.
 * @param eax storage for eax or NULL.
 * @param edx storage for edx or NULL.
 * @return 0 on success, 1 if no x86-msr information is available,
 *     or a negative value on error.
 */
int cpuinfo_feature_x86_extra_msr_get(
        cpuinfo_feature_t* obj,
        uint32_t* index,
        uint32_t* eax,
        uint32_t* edx);

#ifdef __cplusplus
}
#endif /* __cplusplus */
