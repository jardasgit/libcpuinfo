# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright 2023 Tim Wiederhake

libcpuinfo_deps = [
    xml_dep,
]

libcpuinfo_src = [
    'arch/x86.cpp',
    'alias.cpp',
    'architecture.cpp',
    'data.cpp',
    'endianness.cpp',
    'family.cpp',
    'feature.cpp',
    'featureset.cpp',
    'libcpuinfo.cpp',
]

libcpuinfo_tgt = library(
    'cpuinfo',
    libcpuinfo_src,
    dependencies: libcpuinfo_deps,
    include_directories: libcpuinfo_inc,
    install: true,
)

libcpuinfo_dep = declare_dependency(
    link_with: libcpuinfo_tgt,
    sources: libcpuinfo_src,
    dependencies: libcpuinfo_deps,
    include_directories: libcpuinfo_inc,
)

pkg = import('pkgconfig')
pkg.generate(
    libcpuinfo_tgt,
    description: 'CPU data library',
    name: 'libcpuinfo',
)

if get_option('with_tests').disabled()
    subdir_done()
endif

test(
    'alias-test',
    executable(
        'alias-test',
        [
            'alias_test.cpp',
        ],
        dependencies: [
            gtest_dep,
            libcpuinfo_dep,
        ],
    ),
    protocol: 'gtest',
)

test(
    'featureset-test',
    executable(
        'featureset-test',
        [
            'featureset_test.cpp',
        ],
        dependencies: [
            gtest_dep,
            libcpuinfo_dep,
        ],
    ),
    protocol: 'gtest',
)
