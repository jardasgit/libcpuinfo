// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "architecture.hpp"

#include "data.hpp"

std::vector<std::unique_ptr<cpuinfo_architecture_t>>
        cpuinfo_architecture_t::all {};

size_t cpuinfo_architecture_count(void) {
    return cpuinfo_architecture_t::all.size();
}

cpuinfo_architecture_t* cpuinfo_architecture_by_index(size_t index) {
    if (index >= cpuinfo_architecture_t::all.size()) {
        return cpuinfo_error_set_index_oob(__func__);
    }

    return cpuinfo_architecture_t::all[index].get();
}

cpuinfo_architecture_t* cpuinfo_architecture_by_name(
        const char* name,
        const char* domain) {
    return alias_find(__func__, cpuinfo_architecture_t::all, name, domain);
}

void cpuinfo_architecture_free(cpuinfo_architecture_t* /* obj */) {
    /* all architectures are internally allocated and need not be free'd. */
}

const char* cpuinfo_architecture_name_get(
        cpuinfo_architecture_t* obj,
        const char* domain) {
    return alias_get(__func__, obj, domain);
}

size_t cpuinfo_architecture_alias_count(cpuinfo_architecture_t* obj) {
    return alias_count(__func__, obj);
}

cpuinfo_alias_t* cpuinfo_architecture_alias_get(
        cpuinfo_architecture_t* obj,
        size_t index) {
    return alias_index(__func__, obj, index);
}

const char* cpuinfo_architecture_description_get(cpuinfo_architecture_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->description().c_str();
}

cpuinfo_family_t* cpuinfo_architecture_family_get(cpuinfo_architecture_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->family();
}

cpuinfo_endianness_t* cpuinfo_architecture_endianness_get(
        cpuinfo_architecture_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->endianness();
}

int cpuinfo_architecture_wordsize_get(cpuinfo_architecture_t* obj) {
    if (obj == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return -1;
    }

    return obj->wordsize();
}

char* cpuinfo_architecture_xml_get(cpuinfo_architecture_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto string = cpuinfo_data_save([=](xmlNode* node) {
        return cpuinfo_data_encode(node, *obj);
    });

    return cpuinfo_data_strdup(string);
}

void cpuinfo_data_decode(xmlNode* xml, cpuinfo_architecture_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "name") {
            cpuinfo_data_decode(node, value.name());
        } else if (name == "aliases") {
            cpuinfo_data_decode(node, value.aliases());
        } else if (name == "description") {
            cpuinfo_data_decode(node, value.description());
        } else if (name == "family") {
            cpuinfo_data_lookup(node, value.family());
        } else if (name == "endianness") {
            cpuinfo_data_lookup(node, value.endianness());
        } else if (name == "wordsize") {
            cpuinfo_data_decode(node, value.wordsize());
        }
    }
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_architecture_t& value,
        const std::string& name) {
    auto node = cpuinfo_data_create(parent, name);
    cpuinfo_data_encode(node, value.name(), "name");
    cpuinfo_data_encode(node, value.aliases(), "aliases", "alias");
    cpuinfo_data_encode(node, value.description(), "description");
    cpuinfo_data_encode(node, value.family(), "family");
    cpuinfo_data_encode(node, value.endianness(), "endianness");
    cpuinfo_data_encode(node, value.wordsize(), "wordsize");
    return node;
}

int cpuinfo_architecture_equals(
        cpuinfo_architecture_t* lhs,
        cpuinfo_architecture_t* rhs) {
    return lhs == rhs ? CPUINFO_TRUE : CPUINFO_FALSE;
}
