// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "libcpuinfo.hpp"

#include "architecture.hpp"
#include "data.hpp"
#include "endianness.hpp"
#include "family.hpp"
#include "feature.hpp"

#include <cstring>

thread_local static auto g_error_message = std::string {};

void cpuinfo_version(int* major, int* minor, int* patch) {
    if (major != nullptr) {
        *major = CPUINFO_VERSION_MAJOR;
    }

    if (minor != nullptr) {
        *minor = CPUINFO_VERSION_MINOR;
    }

    if (patch != nullptr) {
        *patch = CPUINFO_VERSION_PATCH;
    }
}

static void cpuinfo_host_set() {
    auto alias = cpuinfo_alias_t {};
    alias.name() = "host";

    auto endianness = static_cast<cpuinfo_endianness_t*>(nullptr);

    unsigned int value = 0x01234567U;
    char le[] = { 0x67, 0x45, 0x23, 0x01 };
    char be[] = { 0x01, 0x23, 0x45, 0x67 };

    if (std::memcmp(&value, le, sizeof(value)) == 0) {
        endianness = cpuinfo_endianness_by_name("little", nullptr);
    }

    if (std::memcmp(&value, be, sizeof(value)) == 0) {
        endianness = cpuinfo_endianness_by_name("big", nullptr);
    }

    if (endianness != nullptr) {
        endianness->aliases().push_back(alias);
        cpuinfo_endianness_free(endianness);
    }

    auto architecture_name = static_cast<const char*>(nullptr);

#if defined(CPUINFO_FAMILY_IS_RISCV)
    architecture_name = (sizeof(void*) == 4) ? "RV32" : "RV64";
#elif defined(CPUINFO_FAMILY_IS_X86)
    architecture_name = (sizeof(void*) == 4) ? "x86_32" : "x86_64";
#endif

    auto architecture = cpuinfo_architecture_by_name(
            architecture_name,
            nullptr);
    if (architecture != nullptr) {
        auto family = architecture->family();
        if (family != nullptr) {
            family->aliases().push_back(alias);
            cpuinfo_family_free(family);
        }

        architecture->aliases().push_back(alias);
        cpuinfo_architecture_free(architecture);
    }
}

int cpuinfo_init(void) {
    auto data_dir = std::string { CPUINFO_DATADIR } + '/';

    try {
        cpuinfo_data_load(data_dir + "endianness.xml", [](xmlNode* node) {
            cpuinfo_data_decode(node, cpuinfo_endianness_t::all);
        });

        cpuinfo_data_load(data_dir + "family.xml", [](xmlNode* node) {
            cpuinfo_data_decode(node, cpuinfo_family_t::all);
        });

        cpuinfo_data_load(data_dir + "architecture.xml", [](xmlNode* node) {
            cpuinfo_data_decode(node, cpuinfo_architecture_t::all);
        });

        cpuinfo_data_load(data_dir + "feature.xml", [](xmlNode* node) {
            cpuinfo_data_decode(node, cpuinfo_feature_t::all);
        });

        cpuinfo_host_set();
    } catch (const std::exception& e) {
        cpuinfo_error_set(e.what());
        return -1;
    }

    return 0;
}

void cpuinfo_quit(void) {
}

void cpuinfo_error_clear(void) {
    g_error_message.clear();
}

const char* cpuinfo_error_get(void) {
    return g_error_message.c_str();
}

void cpuinfo_error_set(std::string_view message) {
    g_error_message = message;
}

std::nullptr_t cpuinfo_error_set_func_null(std::string_view func) {
    auto message = std::string {};
    message += func;
    message += " called with NULL obj";

    g_error_message = message;
    return nullptr;
}

std::nullptr_t cpuinfo_error_set_index_oob(std::string_view func) {
    auto message = std::string {};
    message += func;
    message += " called with index out of bounds";

    g_error_message = message;
    return nullptr;
}

void cpuinfo_xml_free(char* obj) {
    if (obj == nullptr) {
        return;
    }

    /* NOLINTNEXTLINE: manual memory management */
    std::free(obj);
}
