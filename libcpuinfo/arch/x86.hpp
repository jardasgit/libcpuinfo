// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "../libcpuinfo.hpp"

#include <libcpuinfo/arch/x86.h>
