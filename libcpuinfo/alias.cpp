// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "alias.hpp"

#include "data.hpp"

void cpuinfo_alias_free(cpuinfo_alias_t* /* obj */) {
    /* all aliases are internally allocated and need not be free'd. */
}

const char* cpuinfo_alias_domain_get(cpuinfo_alias_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->domain().c_str();
}

const char* cpuinfo_alias_name_get(cpuinfo_alias_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->name().c_str();
}

char* cpuinfo_alias_xml_get(cpuinfo_alias_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto string = cpuinfo_data_save([=](xmlNode* node) {
        return cpuinfo_data_encode(node, *obj);
    });

    return cpuinfo_data_strdup(string);
}

void cpuinfo_data_decode(xmlNode* xml, cpuinfo_alias_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "name") {
            cpuinfo_data_decode(node, value.name());
        } else if (name == "domain") {
            cpuinfo_data_decode(node, value.domain());
        }
    }
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_alias_t& value,
        const std::string& name) {
    auto node = cpuinfo_data_create(parent, name);
    cpuinfo_data_encode(node, value.name(), "name");
    cpuinfo_data_encode(node, value.domain(), "domain");
    return node;
}

int cpuinfo_alias_equals(cpuinfo_alias_t* lhs, cpuinfo_alias_t* rhs) {
    return lhs == rhs ? CPUINFO_TRUE : CPUINFO_FALSE;
}
