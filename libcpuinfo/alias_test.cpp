// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "alias.hpp"

#include <gtest/gtest.h>

using namespace std::literals;

struct Klass {
public:
    static std::vector<std::unique_ptr<Klass>> all;

    [[nodiscard]] Klass() noexcept = default;

    [[nodiscard]] const std::string& name() const noexcept {
        return m_name;
    }

    [[nodiscard]] std::string& name() noexcept {
        return m_name;
    }

    [[nodiscard]] const std::vector<cpuinfo_alias_t>& aliases() const noexcept {
        return m_aliases;
    }

    [[nodiscard]] std::vector<cpuinfo_alias_t>& aliases() noexcept {
        return m_aliases;
    }

private:
    std::string m_name {};
    std::vector<cpuinfo_alias_t> m_aliases {};
};

std::vector<std::unique_ptr<Klass>> Klass::all {};

static std::unique_ptr<Klass> make_item(
        const std::string& name,
        std::initializer_list<std::pair<const char*, const char*>> aliases) {
    auto obj = std::make_unique<Klass>();
    obj->name() = name;

    for (auto& [name, domain] : aliases) {
        auto alias = cpuinfo_alias_t {};
        alias.name() = name;
        alias.domain() = domain;
        obj->aliases().push_back(std::move(alias));
    }

    return obj;
}

TEST(Alias, FindCanonical) {
    auto find = []() {
        return alias_find("", Klass::all, "name", nullptr);
    };

    Klass::all.clear();
    EXPECT_EQ(find(), nullptr);

    Klass::all.push_back(make_item("foo", { { "name", "domain" } }));
    EXPECT_EQ(find(), nullptr);

    Klass::all.push_back(make_item("bar", { { "name", "" } }));
    EXPECT_EQ(find(), nullptr);

    Klass::all.push_back(make_item("name", {}));
    EXPECT_EQ(find(), Klass::all.back().get());
}

TEST(Alias, FindEmptyAlias) {
    auto find = []() {
        return alias_find("", Klass::all, "name", "");
    };

    Klass::all.clear();
    EXPECT_EQ(find(), nullptr);

    Klass::all.push_back(make_item("foo", { { "name", "domain" } }));
    EXPECT_EQ(find(), Klass::all.back().get());

    Klass::all.push_back(make_item("bar", { { "name", "" } }));
    EXPECT_EQ(find(), Klass::all.back().get());

    Klass::all.push_back(make_item("name", {}));
    EXPECT_EQ(find(), Klass::all.back().get());
}

TEST(Alias, FindMatchingAlias) {
    auto find = []() {
        return alias_find("", Klass::all, "name", "domain");
    };

    Klass::all.clear();
    EXPECT_EQ(find(), nullptr);

    Klass::all.push_back(make_item("foo", { { "name", "other" } }));
    EXPECT_EQ(find(), Klass::all.back().get());

    Klass::all.push_back(make_item("bar", { { "name", "" } }));
    EXPECT_EQ(find(), Klass::all.back().get());

    Klass::all.push_back(make_item("name", {}));
    EXPECT_EQ(find(), Klass::all.back().get());

    Klass::all.push_back(make_item("baz", { { "name", "domain" } }));
    EXPECT_EQ(find(), Klass::all.back().get());
}

TEST(Alias, Name) {
    auto obj = make_item("a", { { "b", "" }, { "c", "foo" } });

    auto find = [](const std::unique_ptr<Klass>& obj, const char* domain) {
        auto result = alias_get("", obj.get(), domain);
        if (result == nullptr) {
            return std::string_view { "nullptr" };
        }
        return std::string_view { result };
    };

    EXPECT_EQ(find(nullptr, nullptr), "nullptr"sv);

    EXPECT_EQ(find(obj, nullptr), "a"sv);

    EXPECT_EQ(find(obj, ""), "a"sv);

    EXPECT_EQ(find(obj, "foo"), "c"sv);

    EXPECT_EQ(find(obj, "bar"), "a"sv);
}

int main(int argc, char* argv[]) {
    try {
        testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    } catch (const std::exception& e) {
        std::cerr << e.what();
    }

    return 1;
}
