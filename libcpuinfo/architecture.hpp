// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "alias.hpp"
#include "endianness.hpp"
#include "family.hpp"

#include <libcpuinfo/architecture.h>

#include <memory>
#include <string>
#include <vector>

struct cpuinfo_architecture_t {
public:
    static std::vector<std::unique_ptr<cpuinfo_architecture_t>> all;

    [[nodiscard]] cpuinfo_architecture_t() noexcept = default;

    [[nodiscard]] const std::string& name() const noexcept {
        return m_name;
    }

    [[nodiscard]] std::string& name() noexcept {
        return m_name;
    }

    [[nodiscard]] const std::vector<cpuinfo_alias_t>& aliases() const noexcept {
        return m_aliases;
    }

    [[nodiscard]] std::vector<cpuinfo_alias_t>& aliases() noexcept {
        return m_aliases;
    }

    [[nodiscard]] const std::string& description() const noexcept {
        return m_description;
    }

    [[nodiscard]] std::string& description() noexcept {
        return m_description;
    }

    [[nodiscard]] cpuinfo_family_t* const& family() const noexcept {
        return m_family;
    }

    [[nodiscard]] cpuinfo_family_t*& family() noexcept {
        return m_family;
    }

    [[nodiscard]] cpuinfo_endianness_t* const& endianness() const noexcept {
        return m_endianness;
    }

    [[nodiscard]] cpuinfo_endianness_t*& endianness() noexcept {
        return m_endianness;
    }

    [[nodiscard]] const int& wordsize() const noexcept {
        return m_wordsize;
    }

    [[nodiscard]] int& wordsize() noexcept {
        return m_wordsize;
    }

private:
    std::string m_name {};
    std::vector<cpuinfo_alias_t> m_aliases {};
    std::string m_description {};
    cpuinfo_family_t* m_family {};
    cpuinfo_endianness_t* m_endianness {};
    int m_wordsize {};
};

typedef struct _xmlNode xmlNode;
void cpuinfo_data_decode(xmlNode* xml, cpuinfo_architecture_t& value);
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_architecture_t& value,
        const std::string& name = "architecture");
