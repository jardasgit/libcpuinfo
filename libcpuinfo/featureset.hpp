// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "family.hpp"
#include "feature.hpp"

#include <libcpuinfo/featureset.h>

#include <set>
#include <string>

struct cpuinfo_featureset_t {
public:
    [[nodiscard]] cpuinfo_featureset_t() noexcept = default;

    [[nodiscard]] const cpuinfo_family_t* const& family() const noexcept {
        return m_family;
    }

    [[nodiscard]] cpuinfo_family_t*& family() noexcept {
        return m_family;
    }

    [[nodiscard]] const std::set<cpuinfo_feature_t*>&
    features() const noexcept {
        return m_features;
    }

    [[nodiscard]] std::set<cpuinfo_feature_t*>& features() noexcept {
        return m_features;
    }

private:
    cpuinfo_family_t* m_family {};
    std::set<cpuinfo_feature_t*> m_features {};
};

typedef struct _xmlNode xmlNode;
void cpuinfo_data_decode(xmlNode* xml, cpuinfo_featureset_t& value);
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_featureset_t& value,
        const std::string& name = "featureset");
