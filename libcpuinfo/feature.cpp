// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "feature.hpp"

#include "data.hpp"

#include <libcpuinfo/arch/x86.h>

void cpuinfo_data_decode(xmlNode* xml, uint32_t& value) {
    auto string = std::string {};
    cpuinfo_data_decode(xml, string);

    if (string.empty()) {
        value = 0;
        return;
    }

    try {
        value = std::stoul(string, nullptr, 0);
    } catch (...) {
        throw std::invalid_argument {
            "not a valid uint32_t: \"" + string + "\""
        };
    }
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const uint32_t& value,
        const std::string& name) {
    return cpuinfo_data_encode(parent, std::to_string(value), name);
}

static void cpuinfo_data_decode(
        xmlNode* xml,
        cpuinfo_feature_extra_x86_cpuid_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "eax-in") {
            cpuinfo_data_decode(node, value.eax_in);
        } else if (name == "ecx-in") {
            cpuinfo_data_decode(node, value.ecx_in);
        } else if (name == "eax") {
            cpuinfo_data_decode(node, value.eax);
        } else if (name == "ebx") {
            cpuinfo_data_decode(node, value.ebx);
        } else if (name == "ecx") {
            cpuinfo_data_decode(node, value.ecx);
        } else if (name == "edx") {
            cpuinfo_data_decode(node, value.edx);
        }
    }
}

static xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_feature_extra_x86_cpuid_t& value,
        const std::string& name = "x86-cpuid") {
    auto node = cpuinfo_data_create(parent, name);
    cpuinfo_data_encode(node, value.eax_in, "eax-in");
    cpuinfo_data_encode(node, value.ecx_in, "ecx-in");
    cpuinfo_data_encode(node, value.eax, "eax");
    cpuinfo_data_encode(node, value.ebx, "ebx");
    cpuinfo_data_encode(node, value.ecx, "ecx");
    cpuinfo_data_encode(node, value.edx, "edx");

    return node;
}

static void cpuinfo_data_decode(
        xmlNode* xml,
        cpuinfo_feature_extra_x86_msr_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "index") {
            cpuinfo_data_decode(node, value.index);
        } else if (name == "eax") {
            cpuinfo_data_decode(node, value.eax);
        } else if (name == "edx") {
            cpuinfo_data_decode(node, value.edx);
        }
    }
}

static xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_feature_extra_x86_msr_t& value,
        const std::string& name = "x86-msr") {
    auto node = cpuinfo_data_create(parent, name);
    cpuinfo_data_encode(node, value.index, "index");
    cpuinfo_data_encode(node, value.eax, "eax");
    cpuinfo_data_encode(node, value.edx, "edx");

    return node;
}

static void cpuinfo_data_decode(xmlNode* xml, cpuinfo_feature_extra_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "x86-cpuid") {
            auto tmp = cpuinfo_feature_extra_x86_cpuid_t {};
            cpuinfo_data_decode(node, tmp);
            value = tmp;
        } else if (name == "x86-msr") {
            auto tmp = cpuinfo_feature_extra_x86_msr_t {};
            cpuinfo_data_decode(node, tmp);
            value = tmp;
        }
    }
}

static xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_feature_extra_t& value,
        const std::string& name = "extra") {
    if (std::holds_alternative<std::monostate>(value)) {
        return nullptr;
    }

    auto node = cpuinfo_data_create(parent, name);

    if (auto ptr = std::get_if<cpuinfo_feature_extra_x86_cpuid_t>(&value)) {
        cpuinfo_data_encode(node, *ptr, "x86-cpuid");
    }

    if (auto ptr = std::get_if<cpuinfo_feature_extra_x86_msr_t>(&value)) {
        cpuinfo_data_encode(node, *ptr, "x86-msr");
    }

    return node;
}

std::vector<std::unique_ptr<cpuinfo_feature_t>> cpuinfo_feature_t::all {};

size_t cpuinfo_feature_count(void) {
    return cpuinfo_feature_t::all.size();
}

cpuinfo_feature_t* cpuinfo_feature_by_index(size_t index) {
    if (index >= cpuinfo_feature_t::all.size()) {
        return cpuinfo_error_set_index_oob(__func__);
    }

    return cpuinfo_feature_t::all[index].get();
}

cpuinfo_feature_t* cpuinfo_feature_by_name(
        const char* name,
        const char* domain) {
    return alias_find(__func__, cpuinfo_feature_t::all, name, domain);
}

void cpuinfo_feature_free(cpuinfo_feature_t* /* obj */) {
    /* all features are internally allocated and need not be free'd. */
}

const char* cpuinfo_feature_name_get(
        cpuinfo_feature_t* obj,
        const char* domain) {
    return alias_get(__func__, obj, domain);
}

size_t cpuinfo_feature_alias_count(cpuinfo_feature_t* obj) {
    return alias_count(__func__, obj);
}

cpuinfo_alias_t* cpuinfo_feature_alias_get(
        cpuinfo_feature_t* obj,
        size_t index) {
    return alias_index(__func__, obj, index);
}

const char* cpuinfo_feature_description_get(cpuinfo_feature_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->description().c_str();
}

cpuinfo_family_t* cpuinfo_feature_family_get(cpuinfo_feature_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    return obj->family();
}

size_t cpuinfo_feature_features_count(cpuinfo_feature_t* obj) {
    if (obj == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return 0;
    }

    return obj->features().size();
}

cpuinfo_feature_t* cpuinfo_feature_features_get(
        cpuinfo_feature_t* obj,
        size_t index) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    if (index >= obj->features().size()) {
        return cpuinfo_error_set_index_oob(__func__);
    }

    return obj->features()[index];
}

static int cpuinfo_feature_hostsupport_get(
        const cpuinfo_feature_extra_x86_cpuid_t& query) {
    uint32_t eax {};
    uint32_t ebx {};
    uint32_t ecx {};
    uint32_t edx {};

    auto ret = cpuinfo_x86_cpuid(
            query.eax_in,
            query.ecx_in,
            &eax,
            &ebx,
            &ecx,
            &edx);
    if (ret < 0) {
        return -1;
    }

    auto match = (query.eax == (query.eax & eax))
            && (query.ebx == (query.ebx & ebx))
            && (query.ecx == (query.ecx & ecx))
            && (query.edx == (query.edx & edx));
    return match ? 1 : 0;
}

static int cpuinfo_feature_hostsupport_get(
        const cpuinfo_feature_extra_x86_msr_t& query) {
    uint32_t eax {};
    uint32_t edx {};

    auto ret = cpuinfo_x86_msr_read(query.index, &eax, &edx);
    if (ret < 0) {
        return -1;
    }

    auto match = (query.eax == (query.eax & eax))
            && (query.edx == (query.edx & edx));
    return match ? 1 : 0;
}

int cpuinfo_feature_hostsupport_get(cpuinfo_feature_t* obj) {
    if (obj == nullptr) {
        cpuinfo_error_set_func_null(__func__);
        return -1;
    }

    auto& extra = obj->extra();

    if (auto ptr = std::get_if<cpuinfo_feature_extra_x86_cpuid_t>(&extra)) {
        return cpuinfo_feature_hostsupport_get(*ptr);
    }

    if (auto ptr = std::get_if<cpuinfo_feature_extra_x86_msr_t>(&extra)) {
        return cpuinfo_feature_hostsupport_get(*ptr);
    }

    if (obj->features().empty()) {
        return 0;
    }

    for (const auto& feature : obj->features()) {
        auto ret = cpuinfo_feature_hostsupport_get(feature);
        if (ret != 1) {
            return ret;
        }
    }

    return 1;
}

char* cpuinfo_feature_xml_get(cpuinfo_feature_t* obj) {
    if (obj == nullptr) {
        return cpuinfo_error_set_func_null(__func__);
    }

    auto string = cpuinfo_data_save([=](xmlNode* node) {
        return cpuinfo_data_encode(node, *obj);
    });

    return cpuinfo_data_strdup(string);
}

void cpuinfo_data_decode(xmlNode* xml, cpuinfo_feature_t& value) {
    for (auto node : XmlRange { xml }) {
        auto name = cpuinfo_data_node_name(node);

        if (name == "name") {
            cpuinfo_data_decode(node, value.name());
        } else if (name == "aliases") {
            cpuinfo_data_decode(node, value.aliases());
        } else if (name == "description") {
            cpuinfo_data_decode(node, value.description());
        } else if (name == "family") {
            cpuinfo_data_lookup(node, value.family());
        } else if (name == "features") {
            cpuinfo_data_lookup(node, value.features());
        } else if (name == "extra") {
            cpuinfo_data_decode(node, value.extra());
        }
    }
}

xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_feature_t& value,
        const std::string& name) {
    auto node = cpuinfo_data_create(parent, name);
    cpuinfo_data_encode(node, value.name(), "name");
    cpuinfo_data_encode(node, value.aliases(), "aliases", "alias");
    cpuinfo_data_encode(node, value.description(), "description");
    cpuinfo_data_encode(node, value.family(), "family");
    cpuinfo_data_encode(node, value.features(), "features", "feature");
    cpuinfo_data_encode(node, value.extra(), "extra");

    return node;
}

int cpuinfo_feature_equals(cpuinfo_feature_t* lhs, cpuinfo_feature_t* rhs) {
    return lhs == rhs ? CPUINFO_TRUE : CPUINFO_FALSE;
}
