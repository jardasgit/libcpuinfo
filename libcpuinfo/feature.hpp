// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "alias.hpp"
#include "family.hpp"

#include <libcpuinfo/feature.h>

#include <memory>
#include <string>
#include <variant>
#include <vector>

struct cpuinfo_feature_extra_x86_cpuid_t {
    uint32_t eax_in;
    uint32_t ecx_in;
    uint32_t eax;
    uint32_t ebx;
    uint32_t ecx;
    uint32_t edx;
};

struct cpuinfo_feature_extra_x86_msr_t {
    uint32_t index;
    uint32_t eax;
    uint32_t edx;
};

using cpuinfo_feature_extra_t = std::variant<
        std::monostate,
        cpuinfo_feature_extra_x86_cpuid_t,
        cpuinfo_feature_extra_x86_msr_t>;

struct cpuinfo_feature_t {
public:
    static std::vector<std::unique_ptr<cpuinfo_feature_t>> all;

    [[nodiscard]] cpuinfo_feature_t() noexcept = default;

    [[nodiscard]] const std::string& name() const noexcept {
        return m_name;
    }

    [[nodiscard]] std::string& name() noexcept {
        return m_name;
    }

    [[nodiscard]] const std::vector<cpuinfo_alias_t>& aliases() const noexcept {
        return m_aliases;
    }

    [[nodiscard]] std::vector<cpuinfo_alias_t>& aliases() noexcept {
        return m_aliases;
    }

    [[nodiscard]] const std::string& description() const noexcept {
        return m_description;
    }

    [[nodiscard]] std::string& description() noexcept {
        return m_description;
    }

    [[nodiscard]] const cpuinfo_family_t* const& family() const noexcept {
        return m_family;
    }

    [[nodiscard]] cpuinfo_family_t*& family() noexcept {
        return m_family;
    }

    [[nodiscard]] const std::vector<cpuinfo_feature_t*>&
    features() const noexcept {
        return m_features;
    }

    [[nodiscard]] std::vector<cpuinfo_feature_t*>& features() noexcept {
        return m_features;
    }

    [[nodiscard]] const cpuinfo_feature_extra_t& extra() const noexcept {
        return m_extra;
    }

    [[nodiscard]] cpuinfo_feature_extra_t& extra() noexcept {
        return m_extra;
    }

private:
    std::string m_name {};
    std::vector<cpuinfo_alias_t> m_aliases {};
    std::string m_description {};
    cpuinfo_family_t* m_family {};
    std::vector<cpuinfo_feature_t*> m_features {};
    cpuinfo_feature_extra_t m_extra { std::monostate {} };
};

typedef struct _xmlNode xmlNode;
void cpuinfo_data_decode(xmlNode* xml, cpuinfo_feature_t& value);
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_feature_t& value,
        const std::string& name = "feature");
