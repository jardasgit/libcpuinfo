// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include "alias.hpp"

#include <libcpuinfo/family.h>

#include <memory>
#include <string>
#include <vector>

struct cpuinfo_family_t {
public:
    static std::vector<std::unique_ptr<cpuinfo_family_t>> all;

    [[nodiscard]] cpuinfo_family_t() noexcept = default;

    [[nodiscard]] const std::string& name() const noexcept {
        return m_name;
    }

    [[nodiscard]] std::string& name() noexcept {
        return m_name;
    }

    [[nodiscard]] const std::vector<cpuinfo_alias_t>& aliases() const noexcept {
        return m_aliases;
    }

    [[nodiscard]] std::vector<cpuinfo_alias_t>& aliases() noexcept {
        return m_aliases;
    }

    [[nodiscard]] const std::string& description() const noexcept {
        return m_description;
    }

    [[nodiscard]] std::string& description() noexcept {
        return m_description;
    }

private:
    std::string m_name {};
    std::vector<cpuinfo_alias_t> m_aliases {};
    std::string m_description {};
};

typedef struct _xmlNode xmlNode;
void cpuinfo_data_decode(xmlNode* xml, cpuinfo_family_t& value);
xmlNode* cpuinfo_data_encode(
        xmlNode* parent,
        const cpuinfo_family_t& value,
        const std::string& name = "family");
