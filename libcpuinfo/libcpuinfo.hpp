// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#pragma once

#include <libcpuinfo/libcpuinfo.h>

#include <string>

/**
 * Set a new per-thread error message.
 *
 * @param message new per-thread error message.
 */
void cpuinfo_error_set(std::string_view message);

/**
 * Set a new per thread error message indicating an invalid function call
 * with a `nullptr` argument.
 *
 * @param func function name.
 * @return always returns `nullptr`.
 */
std::nullptr_t cpuinfo_error_set_func_null(std::string_view func);

/**
 * Set a new per thread error message indicating an invalid function call
 * with an out of bounds index.
 *
 * @param func function name.
 * @return always returns `nullptr`.
 */
std::nullptr_t cpuinfo_error_set_index_oob(std::string_view func);
